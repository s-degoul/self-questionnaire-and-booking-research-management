<?php

class Alert implements JsonSerializable {
    private $level;
    private $type;
    private $description;
    private $message_admin;
    private $message_subject;
    /* private $status; */
    private $date;
    private $recipients;

    /* public const STATUS_TODO = 1; */
    /* public const STATUS_SOLVED = 2; */
    public const LEVEL_NOTICE = 1;
    public const LEVEL_WARNING = 2;
    public const LEVEL_ERROR = 3;

    public function __construct(int $level = null, string $type = null, string $description = null, string $message_admin = null, string $message_subject = null, DateTime $date = null, string $recipients = EMAILS_ALERTS_TO){
        if ($level){
            $this->setLevel($level);
            $this->setType($type);
            $this->setDescription($description);
            if ($message_admin){
                $this->setMessageAdmin($message_admin);
            } else {
                $this->setMessageAdmin($description);
            }
            if ($message_subject){
                $this->setMessageSubject($message_subject);
            }
            if (! $date){
                $date = new DateTime();
            }
            $this->setDate($date);
            $this->setRecipients($recipients);
        }

        return $this;
    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function __toString(){
        if ($this->getLevel()){
            $str = "level ".$this->getLevel().", type ".$this->getType().", date ".$this->getDate()->format(DATE_FORMAT_LOG).":\n- ".$this->getDescription();
            if ($this->getMessageAdmin()){
                $str .= "\n- ".$this->getMessageAdmin();
            }
            if ($this->getMessageSubject()){
                $str .= "\n- ".$this->getMessageSubject();
            }
        }
        else {
            $str = "void";
        }
        return $str;
    }

    public function getLevel(){
        return $this->level;
    }

    public function getLevelDescription(){
        return self::getPublicLevelDescription($this->level);
    }

    static public function getPublicLevelDescription($level){
        switch ($level){
        case self::LEVEL_NOTICE:
            return "information";
        case self::LEVEL_WARNING:
            return "avertissement";
        case self::LEVEL_ERROR:
            return "erreur";
        default:
            return "inconnu";
        }
    }

    public function getLevelCSS(){
        return self::getPublicLevelCSS($this->level);
    }

    static public function getPublicLevelCSS($level){
        $css = array(
            "icon" => "",
            "text" => ""
        );
        switch ($level){
        case self::LEVEL_NOTICE:
            $css["icon"] = "fa-info";
            $css["text"] = "alert-info";
            break;
        case self::LEVEL_WARNING:
            $css["icon"] = "fa-exclamation-triangle";
            $css["text"] = "alert-warning";
            break;
        case self::LEVEL_ERROR:
            $css["icon"] = "fa-bomb";
            $css["text"] = "alert-danger";
            break;
        default:
            $css["icon"] = "";
        }
        return $css;
    }

    public function setLevel(int $level){
         if (! in_array($level, self::getConstants())){
            throw new Exception("Visit ".$this->__toString().": unknown input level ".$level);
        }
        else {
            $this->level = $level;
        }
        return $this;
    }

    public function getType(){
        return $this->type;
    }

    public function setType(string $type){
        if (! $type){
            throw new InvalidArgumentException("Missing type parameter for alert ".$this->__toString());
        }
        $this->type = $type;
        return $this;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription(string $description){
        if (! $description){
            throw new InvalidArgumentException("Missing description parameter for alert ".$this->__toString());
        }
        $this->description = $description;
        return $this;
    }

    public function getMessageAdmin(){
        return $this->message_admin;
    }

    public function setMessageAdmin(string $message_admin){
        $this->message_admin = $message_admin;
        return $this;
    }

    public function getMessageSubject(){
        return $this->message_subject;
    }

    public function setMessageSubject(string $message_subject){
        $this->message_subject = $message_subject;
        return $this;
    }

    /* public function getStatus(){ */
    /*     return $this->status; */
    /* } */

    /* public function setStatus(int $status){ */
    /*     $this->status = $status; */
    /*     return $this; */
    /* } */

    public function getDate(){
        return $this->date;
    }

    public function setDate(DateTime $date){
        $this->date = $date;
        return $this;
    }

    public function getRecipients(){
        return $this->recipients;
    }

    public function setRecipients(string $recipients){
        $this->recipients = $recipients;
    }

    public function getSubject(array $subjects){
        foreach ($subjects as $subject){
            foreach ($subject->getAlerts() as $alert){
                if ($alert === $this){
                    return $subject;
                }
            }
        }
        return null;
    }

    static private function getConstants(){
        $reflectionClass = new ReflectionClass(__CLASS__);
        return $reflectionClass->getConstants();
    }
}
