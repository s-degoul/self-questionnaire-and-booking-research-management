<?php

class Visit implements JsonSerializable {
    private $id;
    private $type;
    private $date;
    private $status;
    private $survey;
    private $appointment;

    public const STATUS_NOT_SCHEDULED = 0;
    public const STATUS_SCHEDULED = 1;
    public const STATUS_APPT_DONE = 2;
    public const STATUS_COMPLETED_WO_SURVEY = 3;
    public const STATUS_COMPLETED = 4;
    public const STATUS_CANCELED = 5;

    public const TYPE_EXCEPTIONAL = "EXC";
    public const TYPE_UNKNOWN = "UNK";

    public function __construct(){

    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function __toString(){
        $date = "";
        if ($this->getDate()){
            $date = " on ".$this->getDate()->format(DATE_FORMAT_LOG);
        }
        return "id ".$this->getId()." type ".$this->getType().$date;
    }

    public function getPublicDescription(){
        $date = "";
        if ($this->getDate()){
            $date = " le ".$this->getDate()->format(DATE_FORMAT_UI);
        }
        return $this->getTypeDescription().$date;
    }

    public function getId(){
        return $this->id;
    }

    public function setId(string $id){
        $this->id = $id;
    }

    public function getType(){
        return $this->type;
    }

    public function getTypeDescription(){
        return self::getPublicTypeDescription($this->type);
    }

    static public function getPublicTypeDescription($type){
        switch ($type){
        case self:: TYPE_EXCEPTIONAL:
            return "exceptionnelle";
        case self::TYPE_UNKNOWN:
            return "inconnue";
        default:
            return $type;
        }
    }

    public function setType(string $type){
        $this->type = $type;
    }

    public function getDate(){
        return $this->date;
    }

    public function setDate(DateTime $date){
        $this->date = $date;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getStatusDescription(){
        return self::getPublicStatusDescription($this->status);
    }

    static public function getPublicStatusDescription($status){
        switch ($status){
        case self::STATUS_NOT_SCHEDULED:
            return "non programmée";
        case self::STATUS_SCHEDULED:
            return "programmée";
        case self::STATUS_APPT_DONE:
            return "réalisée";
        case self::STATUS_COMPLETED_WO_SURVEY:
            return "complétée sans questionnaire";
        case self::STATUS_COMPLETED:
            return "complétée";
        case self::STATUS_CANCELED:
            return "annulée";
        default:
            return "inconnu";
        }
    }

    public function getStatusCSS(){
        return self::getPublicStatusCSS($this->status);
    }

    static public function getPublicStatusCSS($status){
        $css = array(
            "icon" => "",
            "text" => ""
        );
        switch ($status){
        case self::STATUS_NOT_SCHEDULED:
            $css["icon"] = "fa-question";
            break;
        case self::STATUS_SCHEDULED:
            $css["icon"] = "fa-clock";
            $css["text"] = "waiting";
            break;
        case self::STATUS_APPT_DONE:
            $css["icon"] = "fa-check";
            $css["text"] = "ongoing";
            break;
        case self::STATUS_COMPLETED_WO_SURVEY:
            $css["icon"] = "fa-check";
            $css["text"] = "completed";
            break;
        case self::STATUS_COMPLETED:
            $css["icon"] = "fa-check-double";
            $css["text"] = "completed";
            break;
        case self::STATUS_CANCELED:
            $css["icon"] = "fa-xmark";
            $css["text"] = "canceled";
            break;
        default:
            $css["icon"] = "fa-circle-question";
        }
        return $css;
    }

    public function setStatus(int $status){
        if (! in_array($status, self::getConstants())){
            throw new Exception("Visit ".$this->__toString().": unknown input status ".$status);
        }

        $this->status = $status;
    }

    public function getSurvey(){
        return $this->survey;
    }

    public function setSurvey(Survey $survey){
        $this->survey = $survey;
    }

    public function getAppointment(){
        return $this->appointment;
    }

    public function setAppointment(Appointment $appointment){
        $this->appointment = $appointment;
    }

    public function isNormal(){
        return preg_match("#^V[0-9]+$#", $this->getType());
    }

    static public function getConstants(){
        $reflectionClass = new ReflectionClass(__CLASS__);
        return $reflectionClass->getConstants();
    }
}
