<?php
class Subject implements JsonSerializable {
    private $id;
    private $number;
    private $surname;
    private $firstname;
    private $birthdate;
    private $email;
    private $group;
    private $comment;
    private $visits;
    private $alerts;

    static private $PII_info = array("surname", "firstname", "birthdate", "email");

    public function __construct(){

    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function __toString(){
        return $this->getSurname()." ".$this->getFirstname()." (ID ".$this->getId().")";
    }

    public function __toStringPII(){
        return $this->getPII("surname")." ".$this->getPII("firstname")." (ID ".$this->getPII("id").")";
    }

    /* Get personal identifying information (PII) according to rights of logged user */
    public function getPII($info){
        /* $method = "get".ucfirst($info); */
        $method = "get".str_replace("_", "", ucwords($info, "_"));
        if (! method_exists($this, $method)){
            throw new Exception("Subject ".$this->__toString().": no method found for PII ".$info);
        }
        if (! in_array($info, self::$PII_info)
            or ! session_id()
            or (isset($_SESSION["rights"]) and $_SESSION["rights"]["get_PII"])){
            return $this->$method();
        }
        return "***";
    }

    public function getId(){
        return $this->id;
    }

    public function setId(string $id){
        $this->id = $id;
    }

    public function getNumber(){
        return $this->number;
    }

    public function setNumber(string $number){
        $this->number = $number;
    }

    public function getCompleteId(){
        return $this->getNumber()."-".$this->getId();
    }

    public function getSurname(){
        return $this->surname;
    }

    public function setSurname(string $surname){
        $this->surname = $surname;
    }

    public function getFirstname(){
        return $this->firstname;
    }

    public function setFirstname(string $firstname){
        $this->firstname = $firstname;
    }

    public function getBirthdate(){
        return $this->birthdate;
    }

    public function setBirthdate(string $birthdate){
        $this->birthdate = $birthdate;
        /* if (! preg_match("/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/", $birthdate)){ */
        /*     throw new Exception("Subject ".$this->__toString().": incorrect format for birthdate ".$birthdate); */
        /* } */
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail(string $email){
        /* Accept incorrect mail (it's only the problem of EasyAppointments app) */
        /* if (! filter_var($email, FILTER_VALIDATE_EMAIL)){ */
        /*     throw new Exception("Subject ".$this->__toString().": incorrect format for email ".$email); */
        /* } */
        $this->email = trim($email);
    }

    public function getGroup(){
        return $this->group;
    }

    public function setGroup(string $group){
        $this->group = $group;
    }

    public function getComment(){
        return $this->comment;
    }

    public function setComment(string $comment){
        $this->comment = trim($comment);
    }

    public function getVisits(){
        if (! $this->visits){
            $this->setVisits(array());
        }
        return $this->visits;
    }

    public function setVisits(array $visits){
        $this->visits = $visits;
    }

    public function addVisit(Visit $visit){
        if (! $this->visits){
            $this->setVisits(array());
        }
        $this->visits[] = $visit;
    }

    public function addVisits(array $visits){
        foreach ($visits as $visit){
            $this->addVisit($visit);
        }
    }

    public function deleteVisit(Visit $visit){
        if ($this->visits){
            foreach ($this->visits as $key => $one_visit){
                if ($one_visit === $visit){
                    unset($this->visits[$key]);
                    break;
                }
            }
        }
        else {
            /* FIXME: exception log... */
        }
        return $this;
    }

    public function getAlerts(){
        if (! $this->alerts){
            $this->setAlerts(array());
        }
        return $this->alerts;
    }

    public function setAlerts(array $alerts){
        $this->alerts = $alerts;
    }

    public function addAlert(Alert $alert){
        if (! $this->alerts){
            $this->setAlerts(array());
        }
        if (! $alert->getType()){
            throw new Exception("Subject ".$this->getId().": void alert");
        }
        $this->alerts[] = $alert;
    }

    public function addAlerts(array $alerts){
        foreach ($alerts as $alert){
            $this->addAlert($alert);
        }
    }

    public function createAlert(Alert $alert, array &$alerts){
        $this->addAlert($alert);
        $alerts[] = $alert;
        return $this;
    }

    public function isIncluded(){
        return boolval($this->getSurname());
    }
}
