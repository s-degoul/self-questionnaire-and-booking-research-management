<?php

class Appointment implements JsonSerializable {
    private $id;
    private $subject_surname;
    private $subject_firstname;
    private $subject_email;
    private $subject_birthdate;
    private $investigator_id;
    private $investigator_name;
    private $date;
    private $end_date;
    private $status;
    private $visit_info;
    private $comment;
    private $subject_id;

    static private $PII_info = array("subject_surname", "subject_firstname", "subject_birthdate", "subject_email", "comment");

    public const STATUS_UNKNOWN = 0;
    public const STATUS_SCHEDULED = 1;
    public const STATUS_DONE = 2;
    public const STATUS_CANCELED = 3;

    public const VISIT_INFO_NO_SURVEY = 1;
    public const VISIT_INFO_EXCEPTIONAL = 2;

    public function __construct(){

    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function __toString(){
        return "ID "
            .$this->getId()
            ." customer ".$this->getSubjectFirstname()
            ." ".$this->getSubjectSurname()
            ." on ".$this->getDate()->format(DATE_FORMAT_LOG);
    }

    /* Get personal identifying information (PII) according to rights of logged user */
    public function getPII($info){
        $method = "get".str_replace("_", "", ucwords($info, "_"));
        if (! method_exists($this, $method)){
            throw new Exception("Subject ".$this->__toString().": no method found for PII ".$info);
        }
        if (! in_array($info, self::$PII_info)
            or ! session_id()
            or (isset($_SESSION["rights"]) and $_SESSION["rights"]["get_PII"])){
            return $this->$method();
        }
        return "***";
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getSubjectSurname(){
        return $this->subject_surname;
    }

    public function setSubjectSurname(string $surname){
        $this->subject_surname = trim($surname);
    }

    public function getSubjectFirstname(){
        return $this->subject_firstname;
    }

    public function setSubjectFirstname(string $firstname){
        $this->subject_firstname = trim($firstname);
    }

    public function getSubjectEmail(){
        return $this->subject_email;
    }

    public function setSubjectEmail(string $email){
        /* Accept incorrect mail (it's only the problem of EasyAppointments app) */
        /* if (! filter_var($email, FILTER_VALIDATE_EMAIL)){ */
        /*     throw new Exception("Appointment ".$this->__toString().": incorrect format for email ".$email); */
        /* } */
        $this->subject_email = trim($email);
    }

    public function getSubjectBirthdate(){
        return $this->subject_birthdate;
    }

    public function setSubjectBirthdate($birthdate){
        $this->subject_birthdate = $birthdate;
    }

    public function getInvestigatorId(){
        return $this->investigator_id;
    }

    public function setInvestigatorId(int $investigator_id){
        $this->investigator_id = $investigator_id;
    }

    public function getInvestigatorName(){
        return $this->investigator_name;
    }

    public function setInvestigatorName(string $investigator_name){
        $this->investigator_name = $investigator_name;
    }

    public function getDate(){
        return $this->date;
    }

    public function setDate(DateTime $date){
        $this->date = $date;
    }

    public function getEndDate(){
        return $this->end_date;
    }

    public function setEndDate(DateTime $end_date){
        $this->end_date = $end_date;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getStatusDescription(){
        return self::getPublicStatusDescription($this->status);
    }

    static public function getPublicStatusDescription($status){
        switch ($status){
        case SELF::STATUS_UNKNOWN:
            return "inconnu";
        case SELF::STATUS_SCHEDULED:
            return "programmé";
        case SELF::STATUS_DONE:
            return "réalisé";
        case SELF::STATUS_CANCELED:
            return "annulé";
        default:
            return "inconnu";
        }
    }

    /* Should be private but need to be callable for json import */
    public function setStatus(int $status){
        if (! in_array($status, self::getConstants())){
            throw new Exception("Appointment ".$this->__toString().": unknown input status ".$status);
            return 1;
        }

        $this->status = $status;
    }

    public function setStatusFromComment(){
        $comment_info = $this->getCommentInfo();
        if (isset($comment_info["other"])){
            if ($comment_info["other"] == ABBR_VISIT_CANCELED){
                $this->setStatus(self::STATUS_CANCELED);
                return 0;
            }
        }
        $now = new DateTime();
        $appointment_date = $this->getDate();
        if ($appointment_date){
            if ($appointment_date > $now){
                $this->setStatus(self::STATUS_SCHEDULED);
            }
            elseif (isset($comment_info["subject_id"])){
                $this->setStatus(self::STATUS_DONE);
            }
            else {
                $this->setStatus(self::STATUS_UNKNOWN);
            }
            return 0;
        }
        throw new Exception("Appointment ".$this->__toString().": unable to set status because no date defined");
        return 1;
    }

    public function getVisitInfo(){
        return $this->visit_info;
    }

    public function setVisitInfo(string $visit_info){
        if (! in_array($visit_info, self::getConstants())){
            throw new Exception("Appointment ".$this->__toString().": unknown input visit information ".$visit_info);
            return 1;
        }
        $this->visit_info = $visit_info;
    }

    public function setVisitInfoFromComment(){
        $comment_info = $this->getCommentInfo();
        if (isset($comment_info["other"])){
            if ($comment_info["other"] == ABBR_VISIT_NO_SURVEY){
                $this->setVisitInfo(self::VISIT_INFO_NO_SURVEY);
            }
            else if ($comment_info["other"] == ABBR_VISIT_EXCEPTIONAL){
                $this->setVisitInfo(self::VISIT_INFO_EXCEPTIONAL);
            }
        }
        return 0;
    }

    public function getComment(){
        return $this->comment;
    }

    public function setComment(string $comment){
        $this->comment = trim($comment);
    }

    public function getCommentInfo(){
        $comment = trim($this->getComment());
        $comment_info = array();
        if (! $comment){
            return $comment_info;
        }
        $infos = array_map("trim", explode("\n", $comment));
        $comment_info["subject_id"] = $infos[0];
        if (count($infos) > 1){
            $comment_info["other"] = $infos[1];

            if (count($infos) > 2){
                unset($infos[0]);
                unset($infos[1]);
                throw new Exception("Appointment ".$this->__toString()
                                    .": unkown additional information in comment: "
                                    .implode(" / ", $infos));
            }
        }
        return $comment_info;
    }

    public function getSubjectId(){
        return $this->subject_id;
    }

    public function setSubjectId(string $subject_id){
        $this->subject_id = $subject_id;
    }

    public function setSubjectIdFromComment(){
        $comment_info = $this->getCommentInfo();
        if (isset($comment_info["subject_id"])){
            $this->setSubjectId($comment_info["subject_id"]);
        }
    }

    public function setInfoFromComment(){
        $this->setStatusFromComment();
        $this->setSubjectIdFromComment();
        $this->setVisitInfoFromComment();
    }

    static private function getConstants(){
        $reflectionClass = new ReflectionClass(__CLASS__);
        return $reflectionClass->getConstants();
    }
}
