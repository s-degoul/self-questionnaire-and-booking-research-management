<?php

/* Minimum adaptations = replace "my_" values by suitable ones */



/* Mark config as loaded */
define("CONFIG_LOADED", 1);

/* Mode: dev or prod */
define("MODE", "prod");

/* EasyAppointments API features */
define("EA_API_USER", "my_EA_user");
define("EA_API_PASSWORD", "my_EA_password");
define("EA_API_URL", "my_EA_API_URL");
/* EasyAppointments URL for back office */
define("EA_ADMIN_URL", "my_EA_backend_URL");
/* Define if user login through EA credentials */
define("LOGIN_WITH_EA", "__EA__");

/* LimeSurvey */
/* Root URL */
define("LS_ROOT_URL", "my_LS_URL");
/* URL of back office */
define("LS_ADMIN_URL", "my_LS_backend_URM");
/* Survey ID */
define("LS_SURVEY_ID", "my_LS_survey_ID");
/* API features */
/* User: preferably with limited privileges!! */
define("LS_API_USER", "my_LS_user");
define("LS_API_PASSWORD", "my_LS_password");
define("LS_API_URL", LS_ROOT_URL."admin/remotecontrol");
/* SGQA identifier of question corresponding to subject name initials (optional) */
/* For SGQA, see https://manual.limesurvey.org/SGQA_identifier */
define("LS_Q_SUBJECT_NAME_INITIALS", LS_SURVEY_ID."Xmy_group_IDXmy_question_ID");

/* URL of study website */
define("STUDY_URL", "my_study_URL");


/* Abbreviations used in appointments' comment */
/* - visit canceled */
define("ABBR_VISIT_CANCELED", "ND");
/* - no survey completion */
define("ABBR_VISIT_NO_SURVEY", "NS");
/* - exceptional visit */
define("ABBR_VISIT_EXCEPTIONAL", "EXC");


/* Format of dates */
/* - in user interface, complete */
define("DATE_FORMAT_UI", "d/m/Y H\hi");
/* - in user interface, short */
define("DATE_FORMAT_UI_SHORT", "d/m/Y");
/* - in logs */
define("DATE_FORMAT_LOG", "Y-m-d H:i:s");
/* - in file name */
define("DATE_FORMAT_FILE", "Y.m.d_H.i.s");
/* - in iCalendar output */
define("DATE_FORMAT_ICAL", "Ymd\THis");


/* Files */
/* - data directory */
define("DIR_DATA", "data/");
/* - tmp files directory */
define("DIR_TMP", "tmp/");
/* - subjects list */
define("FILE_SUBJECTS_LIST", DIR_DATA."subjects_list.csv");
/* - backups */
define("FILE_BACKUP_NAME_FORMAT", "backup_{date}.json");
/* - logs */
define("FILE_LOG", "data/logs");
/* - LaTeX model for subjects' sheet */
define("FILE_SUBJECT_SHEET_MODEL", "assets/subjects_sheet_model.tex");
/* - QR code for study website */
define("FILE_QR_STUDY_BASENAME", "study_website");
/* - Common name of subjects' sheets */
define("FILE_SUBJECT_SHEET_COMMON", "subject_sheet_");
/* - File including all subjects' sheets */
define("FILE_SUBJECTS_SHEETS", DIR_DATA."subjects_sheet.pdf");

/* Separator for CSV files */
define("SEP_CSV", ";");

/* Number of "normal" visits (i.e.provided for by study protocol) for each subject */
define("NB_VISITS", 3);
/* Variables in subjects list file */
/* - subject id */
define("SUBJECTS_LIST_VAR_ID", "id");
/* - subject number */
define("SUBJECTS_LIST_VAR_NUMBER", "name");
/* - common name for variables relative to token  */
define("SUBJECTS_LIST_VAR_SURVEY_TOKEN", "survey_token_");
/* - comment */
define("SUBJECTS_LIST_VAR_COMMENT", "comment");
/* - group (optional) */
define("SUBJECTS_LIST_VAR_GROUP", "group");

/* For DateInterval format, see: https://www.php.net/manual/fr/dateinterval.construct.php */
/* Maximum time (format: DateInterval) between visit and survey completion before raising an alert */
define("MAX_TIME_VISIT_SURVEY_COMPLETION_ALERT", "PT24H");
/* Maximum acceptable time (format: DateInterval) between visit and survey completion */
define("MAX_TIME_VISIT_SURVEY_COMPLETION_ACCEPT", "PT48H");
/* Minimum time (format: DateInterval) between to "nomal" visits */
define("MIN_TIME_TWO_VISITS", "P1M21D");
/* Maximum time (format: DateInterval) between to "normal" visits */
define("MAX_TIME_TWO_VISITS", "P2M7D");

/* Emails */
/* Addresses must be compatible with PHP mail() function https://www.php.net/manual/en/function.mail.php */
/* Name of expeditor */
define("EMAIL_FROM_NAME", "my_email_from_name");
/* Email address of expeditor */
define("EMAIL_FROM_ADDRESS", "my_email_from_address");
/* Email address to reply to */
define("EMAIL_REPLY_TO", "my_email_reply_to_address");
/* Email address(es) to send alerts to */
define("EMAILS_ALERTS_TO", "my_alerts_recipients_mail_concatenated_with_comma");
/* Email address(es) to send alerts about inclusions to */
define("EMAILS_ALERTS_INCLUSION_TO", "my_inclusion_alerts_recipients_mail_concatenated_with_comma");


/* Users */
$users = array(
    array(
        "name" => "my_user_1_name",
        "username" => "my_user_1_login",
        "password" => LOGIN_WITH_EA,
        "password_rescue" => "my_user_1_password",
        "rights" => array(
            "get_PII" => TRUE,
            "launch_dm" => TRUE,
            "get_all_rando" => TRUE,
        ),
        "ical" => array(
            array(
                "name" => "my_cal_1_name",
                "key" => "my_cal_1_key",
                "EA_provider_ids" => array("my_some_EA_provider(s)_id")
            ),
            array(
                "name" => "my_cal_2_name",
                "key" => "my_cal_2_key",
                "EA_provider_ids" => array("my_other_EA_provider(s)_id")
            )
        ),
    ),
);
