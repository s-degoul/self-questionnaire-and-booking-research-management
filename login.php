<?php
session_start();

include_once("config.php");
include_once("functions.php");

if (isset($_REQUEST["submit"])){
    $username = stripslashes($_REQUEST["username"]);
    $password = stripslashes($_REQUEST["password"]);

    foreach($users as $user){
        if ($user["username"] == $username){
            if ($user["password"] == LOGIN_WITH_EA){
                if (! test_EA_API($username, $password)){
                    if (isset($user["password_rescue"])){
                        if ($user["password_rescue"] != $password){
                            break;
                        }
                    }
                    else {
                        break;
                    }
                }
            }
            elseif ($user["password"] != $password){
                break;
            }
            $_SESSION["username"] = $user["username"];
            $_SESSION["name"] = $user["name"];
            $_SESSION["rights"] = $user["rights"];
            if (isset ($user["ical"])){
                $_SESSION["ical"] = $user["ical"];
            }
            else {
                $_SESSION["ical"] = array();
            }

            add_log("User ".$user["name"]." logged in", "NOTICE");
            header("Location:index.php");
            exit;
        }
    }
}

include("header.html");
?>

<h2>Connexion</h2>

<?php
if(isset($_REQUEST["submit"])){
?>
<p class="alert alert-warning">
    Identifiants invalides
</p>
<?php
}
?>

<form action="" method="post">
    <input type="text" name="username" placeholder="Nom d'utilisateur" />
    <input type="password" name="password" placeholder="Mot de passe" />
    <input type="submit" name="submit" value="Connexion" />
</form>

<?php
include("footer.html");
?>
