<?php

session_start();
if (! isset($_SESSION["username"])){
    header("Location:login.php");
    exit;
}

include_once("config.php");
include_once("functions.php");

$dm = load_dm("last");

$file = null;
/* If subject_id parameter, sheet specific to this subject */
if (isset($_REQUEST["subject_id"])){
    $subject_id = stripslashes($_REQUEST["subject_id"]);
    foreach($dm["subjects"] as $subject){
        if ($subject->getId() == $subject_id){
            if ($subject->isIncluded()  or $_SESSION["rights"]["get_all_rando"]){
                $file = DIR_DATA.FILE_SUBJECT_SHEET_COMMON.$subject_id.".pdf";
            }
            break;
        }
    }
}
/* Else, all subject sheets, if "get_all_rando" privilege */
else if ($_SESSION["rights"]["get_all_rando"]){
    $file = FILE_SUBJECTS_SHEETS;
}

if (! $file or ! file_exists($file)){
    die("Access forbidden or file not found");
}

header("Content-Type: application/pdf; charset=utf-8");
header("Content-Length: ".filesize($file));
header("Content-Disposition: inline; filename=".basename($file));
readfile($file);
