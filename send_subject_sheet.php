<?php
session_start();
if (! isset($_SESSION["username"])){
    header("Location:login.php");
    exit;
}
if (! $_SESSION["rights"]["get_PII"]){
    die("Access denied");
}

include_once("config.php");
include_once("functions.php");

if (! isset($_REQUEST["subject_id"])
    or ($subject_id = stripslashes($_REQUEST["subject_id"])) == FALSE){
    die("No subject ID provided");
}

$error = null;
if (isset($_REQUEST["submit"])){
    $subject_email = stripslashes($_REQUEST["subject_email"]);
    $subject_email_other = stripslashes($_REQUEST["subject_email_other"]);

    if (! $subject_email and ! $subject_email_other){
        $error = "Aucune adresse email choisie ou saisie manuellement";
    }
    elseif ($subject_email and $subject_email_other){
        $error = "Une seule adresse email doit être choisie ou saisie manuellement";
    }
    $subject_email = $subject_email ? $subject_email : $subject_email_other;
    if (! filter_var($subject_email, FILTER_VALIDATE_EMAIL)){
        $error = "L'adresse mail choisie est invalide";
    }

    if (! $error){
        $subject_sheet_file = DIR_DATA.FILE_SUBJECT_SHEET_COMMON.$subject_id.".pdf";
        // File contents
        $subject_sheet_content = file_get_contents($subject_sheet_file);
        $subject_sheet_content = chunk_split(base64_encode($subject_sheet_content));
        $subject_sheet_filename = basename($subject_sheet_file);
        $boundary = md5(uniqid(time()));

        $email_subject = "Étude HERBAS : votre fiche personnelle";
        $email_message = "Bonjour.\r\n\r\n"
                       ."Vous trouverez ci-joint votre fiche personnelle de participation à l'étude HERBAS.\r\n\r\n"
                       ."Elle contient les informations suivantes \r\n"
                       ."- déroulement de l'étude : dates des visites, prise de l'aromathérapie,\r\n"
                       ."- liens vers les questionnaires en ligne de chaque visite (à renseigner *après* la visite),\r\n"
                       ."- lien vers le site de l'étude.\r\n\r\n"
                       ."En cas de problème ou question, n'hésitez pas à contacter le médecin qui vous a inclus dans l'étude (voir la rubrique \"Contacts\" sur le site de l'étude ".STUDY_URL.").\r\n\r\n"
                       ."*Important* : merci de vérifier que le numéro d'anonymat est bien le même que celui de la version papier qui vous a été donnée. Si ce n'est pas le cas, merci de nous le signaler au plus vite, avant de renseigner les questionnaires en ligne.";

        $email_header = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDRESS.">\r\n"
                      ."Reply-To: ".EMAIL_REPLY_TO."\r\n"
                      ."MIME-Version: 1.0\r\n"
                      ."Content-Type: multipart/mixed; boundary=\"".$boundary."\"\r\n\r\n";

        $email_content = "--".$boundary."\r\n"
                       ."Content-Type:text/plain; charset=utf-8\r\n"
                       ."Content-Transfer-Encoding: quoted-printable\r\n\r\n"
                       .quoted_printable_encode($email_message)."\r\n\r\n"
                       ."--".$boundary."\r\n"
                       ."Content-Type: application/pdf; name=\"".$subject_sheet_filename."\"\r\n"
                       ."Content-Transfer-Encoding: base64\r\n"
                       ."Content-Disposition: attachment; filename=\"".$subject_sheet_filename."\"\r\n"
                       ."X-Attachment-Id: ".rand(1000, 99999)."\r\n\r\n"
                       .$subject_sheet_content."\r\n\r\n"
                       ."--".$boundary."--";

        if (mail($subject_email, $email_subject, $email_content, $email_header)) {
            add_log("Subject sheet sent to ".$subject_email, "NOTICE");
            $_SESSION["message"] = "Fiche personnelle envoyée par mail à ".$subject_email;
            header("Location:index.php");
            exit;
        } else {
            $error = "Échec de l'envoi du mail";
        }
    }
}

$dm = load_dm("last");

include("header.html");

$subject_email = null;
foreach ($dm["subjects"] as $subject){
    if ($subject->getId() == $subject_id){
        if ($subject->getEmail()){
            $subject_email = $subject->getEmail();
        }
        echo "<h2>Envoi de la fiche au sujet ".$subject->getCompleteId();
        if ($subject->getSurname()){
            echo " (".$subject->getSurname()." ".$subject->getFirstname().")";
        }
        echo "</h2>";
        break;
    }
}
if ($error){
?>
<p class="alert alert-warning">
    <?php echo $error; ?>
</p>
<?php
}
?>


<p>
    Choisissez l&apos;adresse email de destination :
</p>
<form action="" method="post">

<?php
if ($subject_email){
?>
    <input type="radio" name="subject_email" id="subject_email" value="<?php echo $subject_email; ?>" checked="checked" />
    <label for="subject_email"><?php echo $subject_email; ?></label>
<?php
        }
else {
    $i = 0;
    $sep = "";
?>
    <p class="alert alert-info">
        Aucune adresse mail rattachée au sujet pour l&apos;instant.
        <br/>
        Choisissez-en une parmi celles des rendez-vous "orphelins" <strong>(ne vous trompez pas d&apos;adresse mail !!)</strong> ou spécifiez-en une manuellement.
    </p>
<?php
    foreach($dm["appointments"] as $appointment){
        $i++;
        echo $sep;
?>
    <input type="radio" name="subject_email" id="subject_email_<?php echo $i; ?>" value="<?php echo $appointment->getSubjectEmail(); ?>" />
    <label for="subject_email_<?php echo $i; ?>"><?php echo $appointment->getSubjectEmail()." (".$appointment->getSubjectSurname()." ".$appointment->getSubjectFirstname().")"; ?></label>
<?php
         $sep = "<br/>";
    }
}
?>
    <br/>
    <input type="radio" name="subject_email" id="subject_email_none" value=""<?php echo $subject_email ? "" : " checked=\"checked\""; ?> />
    <label for="subject_email_none">Autre</label>
    <input type="text" name="subject_email_other" size=30 placeholder="adresse@mail" />

    <br/><br/>
    <input type="hidden" value="<?php echo $subject_id; ?>" />
    <button type="submit" class="btn btn-success" name="submit" value="1">
        Envoyer
    </button>
    <a href="index.php" class="btn btn-primary">
        <span class="fas fa-arrow-left"></span> Annuler
    </a>
</form>

<?php
include("footer.html");
?>
