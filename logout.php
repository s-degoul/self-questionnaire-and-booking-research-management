<?php

session_start();

include_once("config.php");
include_once("functions.php");

if (session_destroy()) {
    add_log("User ".$_SESSION["name"]." logged out", "NOTICE");
    header("Location: login.php");
    exit;
}
