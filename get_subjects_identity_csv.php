<?php
session_start();
if (! isset($_SESSION["username"])){
    header("Location:login.php");
    exit;
}
if (! $_SESSION["rights"]["get_PII"]){
    http_response_code(403);
    die("Forbidden");
}

include_once("config.php");
include_once("functions.php");

/* Loading data from data management */
$dm_date = "last";
if (isset($_GET["dm_date"])){
    $dm_date = stripslashes(urldecode($_GET["dm_date"]));
}
$dm = load_dm($dm_date);

/* Ordering information */
usort($dm["subjects"], function($subject1, $subject2){
    $subject1_id_int = get_int_from_id($subject1->getNumber());
    $subject2_id_int = get_int_from_id($subject2->getNumber());
    if ($subject1_id_int != $subject2_id_int){
        return ($subject1_id_int < $subject2_id_int) ? -1 : 1;
    }
    return ($subject1->getCompleteId() < $subject2->getCompleteId()) ? -1 : 1;
});

header("Content-Type: text/csv; charset=utf-8");
header("Content-Disposition: attachment; filename=subjects_identity.csv");

/* Excel compatibility */
echo chr(0xEF).chr(0xBB).chr(0xBF);

/* CSV columns headers */
echo "Numéro".SEP_CSV."ID aléatoire".SEP_CSV."Nom".SEP_CSV."Prénom".SEP_CSV."Date de naissance".PHP_EOL;

/* One line per subject */
foreach ($dm["subjects"] as $subject){
    echo $subject->getNumber()
        .SEP_CSV
        .$subject->getId().SEP_CSV
        .$subject->getSurname().SEP_CSV
        .$subject->getFirstname().SEP_CSV
        .$subject->getBirthdate()
        .PHP_EOL;
}
