<?php
session_start();
if (! isset($_SESSION["username"])){
    header("Location:login.php");
    exit;
}

include_once("config.php");
include_once("functions.php");

/* Loading data from data management */
$dm_date = "last";
if (isset($_REQUEST["submit"])){
    $dm_date = stripslashes($_REQUEST["dm_date"]);
}
$dm = load_dm($dm_date);

/* Ordering information */
/* Subjects: by Number */
usort($dm["subjects"], function($subject1, $subject2){
    $subject1_id_int = get_int_from_id($subject1->getNumber());
    $subject2_id_int = get_int_from_id($subject2->getNumber());
    if ($subject1_id_int != $subject2_id_int){
        return ($subject1_id_int < $subject2_id_int) ? -1 : 1;
    }
    return ($subject1->getCompleteId() < $subject2->getCompleteId()) ? -1 : 1;
});
/* Orphan appointments: by date */
usort($dm["appointments"], sort_objects_by_date("getDate"));
/* Alerts: by level & date */
usort($dm["alerts"], function($alert1, $alert2){
    if ($alert1->getLevel() != $alert2->getLevel()){
        return ($alert1->getLevel() > $alert2->getLevel()) ? -1 : 1;
    }
    return ($alert1->getDate() < $alert2->getDate()) ? -1 : 1;
});


/* DEBUG */
/* print_output($dm); */

include("header.html");
?>

<div class="row">
    <div class="col-sm-4">
        <p>
            <strong>Utilisateur :</strong> <?php echo $_SESSION["name"]; ?>.
            <a href="logout.php">Se déconnecter</a>
        </p>
    </div>
    <div class="col-sm-8 ressources">
        <p>
            <a href="<?php echo EA_ADMIN_URL; ?>" target="_blank" title="EasyAppointments"><span class="fa-solid fa-calendar-days"></span>&nbsp;Planning des rendez-vous</a> |
            <a href="<?php echo STUDY_URL; ?>" target="_blank" title="Site de la DRC"><span class="fa-solid fa-building-columns"></span>&nbsp;Site de l&apos;étude</a> |
            <a href="<?php echo LS_ADMIN_URL; ?>" target="_blank" title="LimeSurvey"><span class="fa-solid fa-square-poll-horizontal"></span>&nbsp;Gestion du questionnaire en ligne</a>
        </p>
<?php
    if ($_SESSION["ical"]){
?>
        <details data-popover="bottom">
            <summary>
                Lien iCalendar <a href="https://fr.wikipedia.org/wiki/ICalendar"><span class="fa-brands fa-wikipedia-w" style="font-size: 0.5em;" title="Voir la définition"></span></a> du planning des rendez-vous
            </summary>
            <div>
                <ul>
<?php
        foreach($_SESSION["ical"] as $config_ical){
            echo "<li><small>".$config_ical["name"]." : ".str_replace("index.php", "", url())
                ."appointments_ical.php?key="
                .$config_ical["key"]."</small></li>";
        }
?>
                </ul>
            </div>
        </details>
<?php
    }
?>
    </div>
</div>

<hr/>

<?php
if (empty($dm["subjects"])){
?>
<p class="alert alert-info">
    <span class="fa-solid fa-circle-info"></span> Pas de <i>data-management</i> disponible
</p>
<?php
    if ($_SESSION["rights"]["launch_dm"]){
?>
<p>
    <a href="data_management.php" onclick="return confirm('Lancer un DM ? Ceci enverra les éventuelles alertes.')">
        <span class="fa-solid fa-rotate"></span> Lancer un <i>data-management</i>
    </a>
</p>
<?php
    }
    include("footer.html");
    exit;
}

if (isset ($_SESSION["message"]) and ! empty($_SESSION["message"])){
?>
    <p class="alert alert-info">
        <span class="fa-solid fa-envelope-circle-check"></span> <?php echo $_SESSION["message"]; ?>
    </p>
<?php
    unset($_SESSION["message"]);
}
?>

<div class="row">
    <div class="dm-list col-sm-6">
<?php
$dm_files = get_dm_files();
usort($dm_files, function($dm_file1, $dm_file2){
    if($dm_file1["date"] == $dm_file2["date"]){
        return 0;
    }
    return ($dm_file1["date"] > $dm_file2["date"]) ? -1 : 1;
});
?>
<form action="" method="post">
    <details data-popover="bottom">
        <summary>
<?php
$i = 0;
$dm_file_date = "";
foreach ($dm_files as $dm_file){
    $i ++;
    if ($dm_date == $dm_file["date"]->format(DATE_FORMAT_FILE)
        or ($dm_date == "last" and $i == 1)){
        $dm_file_date = $dm_file["date"]->format(DATE_FORMAT_UI);
        break;
    }
}
?>
            <strong>Date du <i>data-management</i>&nbsp;:</strong> <?php echo $dm_file_date; ?>
        </summary>
        <div>
        <div class="form-group row">
            <label for="dm_date">Changer la date</label>
            <div class="col-md-6">
                <select name="dm_date" class="form-select">
<?php
$i = 0;
foreach ($dm_files as $dm_file){
    $i ++;
    $selected = "";
    if ($dm_date == $dm_file["date"]->format(DATE_FORMAT_FILE)
        or ($dm_date == "last" and $i == 1)){
        $selected = " selected";
    }
?>
                <option value="<?php echo ($i == 1) ? "last" : $dm_file["date"]->format(DATE_FORMAT_FILE); ?>" <?php echo $selected; ?>>
                    <?php echo $dm_file["date"]->format(DATE_FORMAT_UI); ?>
                </option>
<?php
}
?>
                </select>
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-success" name="submit" value="1">
                OK
                </button>
            </div>

<?php
if ($_SESSION["rights"]["launch_dm"]){
?>
            <p>
                <a href="data_management.php" onclick="return confirm('Lancer un DM ? Ceci enverra les éventuelles alertes.')">
                    <span class="fa-solid fa-rotate"></span> Lancer un <i>data-management</i>
                </a>
            </p>
<?php
}
?>
        </div>
        </div>
    </details>
</form>

    </div>
    <div class="helpers col-sm-6">
        <p><strong>Dates clé :</strong></p>
        <ul>
<?php
    $date_ref = new DateTime();
    $date_ref->add(new DateInterval("P2M"));
?>
            <li>Dans 2 mois : <?php echo $date_ref->format(DATE_FORMAT_UI_SHORT); ?></li>
<?php
    $date_ref->add(new DateInterval("P2M"));
?>
            <li>Dans 4 mois : <?php echo $date_ref->format(DATE_FORMAT_UI_SHORT); ?></li>
        </ul>
    </div>
</div>


<?php
if ($dm_date != "last"){
?>
<p class="alert alert-info">
    <span class="fa-solid fa-circle-info"></span> Vous consultez les données d&apos;un data-management ancien
</p>
<?php
}
?>



<!--
--------
Subjects
--------
-->

<h2>Sujets</h2>

<div class="row">
    <div class="col-md-3">
        <button type="button" class="btn btn-primary" value="1" id="toggle-display-subjects">
            <span id="toggle-display-subjects-icon" class="fa-solid fa-eye-slash"></span> <span id="toggle-display-subjects-label">Masquer les sujets non inclus</span>
        </button>
    </div>
    <div class="col-md-3">
<?php
$sep = "";
if ($_SESSION["rights"]["get_PII"]){
?>
        <a href="get_subjects_identity_csv.php?dm_date=<?php echo urlencode($dm_date); ?>">
            <span class="fa-solid fa-file-csv" title="format CSV"></span>
            Liste de correspondances
        </a>
<?php
    $sep = "<br/>";
}
if ($_SESSION["rights"]["get_all_rando"]){
    echo $sep;
?>
        <a href="get_subject_sheet.php">
            <span class="fa-solid fa-file-pdf" title="format PDF"></span>
            Toutes les fiches sujet
        </a>
<?php
}
?>
    </div>
    <div class="col-md-3">
        <details data-popover="bottom" class="legend">
            <summary>
                Légende
            </summary>
            <div>
                <dl>
                    <dt>Visite</dt>
                        <dd>
                            <ul>
<?php
        foreach(Visit::getConstants() as $constant_name => $constant_value){
            if (preg_match("#^STATUS_#", $constant_name)){
                $css = Visit::getPublicStatusCSS($constant_value);
?>
                <li class="<?php echo $css["text"]; ?>">
                    <span class="fa-solid <?php echo $css["icon"]; ?>"></span> <?php echo Visit::getPublicStatusDescription($constant_value); ?>
                </li>
<?php
            }
        }
?>
                            </ul>
                        </dd>

                        <dt>Hors délais</dt>
                        <dd>
                            <ul>
                                <li class="icon-alert-info">
                                    <span class="fa-solid fa-clock"></span> évènement réalisé
                                </li>
                                <li class="icon-alert-warning">
                                    <span class="fa-solid fa-clock"></span> évènement à réaliser
                                </li>
                            </ul>
                        </dd>

                    <dt>Divers</dt>
                    <dd>
                        <ul>
                            <li>
                                <span class="fa-solid fa-circle-plus"></span> cliquer pour afficher des détails
                            </li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </details>
    </div>
    <div class="col-md-3">
        <details data-popover="bottom" class="legend">
            <summary>
                Statistiques
            </summary>
<?php
$nb_inclusions = 0;
$nb_visits = array();
$test_subject_number_regex = "#^TEST-[0-9]{3}$#";
foreach ($dm["subjects"] as $subject){
    if (preg_match($test_subject_number_regex, $subject->getNumber())){
        continue;
    }
    if ($subject->isIncluded()){
        $nb_inclusions ++;

        for ($i = 0; $i < NB_VISITS; $i ++){
            $visit_type = "V".($i + 1);
            if (! array_key_exists($visit_type, $nb_visits)){
                $nb_visits[$visit_type] = array();
            }
            $visit = null;
            foreach ($subject->getVisits() as $visit){
                if ($visit->getType() == $visit_type){
                    break;
                }
            }
            /* NB: empty visit ignored, no error (because already handled thereafter) */
            if ($visit){
                $visit_status = $visit->getStatus();
                if (! array_key_exists($visit_status, $nb_visits[$visit_type])){
                    $nb_visits[$visit_type][$visit_status] = 0;
                }
                $nb_visits[$visit_type][$visit_status] ++;
            }
        }
    }
}
?>
            <div>
                <p><small>En excluant les sujets test (format du numéro sujet : <?php echo $test_subject_number_regex; ?>)</small></p>
                <dl>
                    <dt>Nombre d&apos;inclusions</dt>
                    <dd>
                        <?php echo $nb_inclusions; ?>
                    </dd>
                </dl>
<?php
foreach ($nb_visits as $visit_type => $visit_status_nb){
?>
                <dl>
                    <dt>Visite <?php echo $visit_type; ?></dt>
                    <dd>
                        <ul>
<?php
    foreach ($visit_status_nb as $visit_status => $visit_nb){
?>
                            <li><?php echo Visit::getPublicStatusDescription($visit_status); ?> : <?php echo $visit_nb; ?></li>
<?php
    }
?>
                        </ul>
                    </dd>
                </dl>
<?php
}
?>
            </div>
        </details>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-sm">
        <thead class="table-dark">
            <tr>
                <th scope="col">Numéro</th>
                <th scope="col">ID</th>
                <th scope="col">Groupe</th>
                <th scope="col">Identité<br/>Nom Prénom</th>
                <th scope="col">Date de<br/>naissance</th>
                <th scope="col">Courriel</th>
                <th scope="col">Commentaires</th>
<?php
for ($i = 0; $i < NB_VISITS; $i ++){
?>
                <th scope="col">Visite <?php echo $i + 1; ?></th>
<?php
}
?>
                <th scope="col">Autres<br/>visites</th>
                <th scope="col">Alertes</th>
            </tr>
        </thead>
        <tbody>
<?php
foreach ($dm["subjects"] as $subject){
    /* Highlighting subjects with alerts in recent data management */
    $css_row = "";
    $css_icon = "";
    $max_alert_level = 0;
    foreach ($subject->getAlerts() as $alert){
        if (in_array($alert, $dm["alerts"])){
            if ($alert->getLevel() > $max_alert_level){
                $max_alert_level = $alert->getLevel();
                $css_row = str_replace("alert", "bg", $alert->getLevelCSS()["text"]);
                $css_icon = "fa-solid ".$alert->getLevelCSS()["icon"];
            }
        }
    }
    if (! $subject->isIncluded()){
        $css_row .= " subject-not-included";
    }
?>
            <tr class="<?php echo $css_row; ?>">
                <th scope="row" class="subject-number" title="double-cliquez pour (dé)sélectionner"><?php echo $subject->getNumber(); ?><br/><span class="fa-solid fa-highlighter highlight-subject"></span><span class="<?php echo $css_icon ?>"></span></th>
                <th scope="row"><?php echo $subject->getId(); ?></th>
                <td class="subject-group">
<?php
    $send_subject_sheet_label = "@<small>envoyer</small>";
    if ($subject->isIncluded()
        or $_SESSION["rights"]["get_all_rando"]){
        echo str_replace("|", "<br/>", $subject->getGroup());
?>
                    <br/>
                    <small><a href="get_subject_sheet.php?subject_id=<?php echo $subject->getId(); ?>" target="_blank">Fiche</a></small><?php
        $send_subject_sheet_label = "@";
    }
    if ($_SESSION["rights"]["get_PII"]){
?>
                    &nbsp;<a href="send_subject_sheet.php?subject_id=<?php echo $subject->getId(); ?>" title="Envoyer la fiche sujet par mail"><?php echo $send_subject_sheet_label; ?></a>
<?php
    }
?>
                </td>
                <td><?php echo $subject->getPII("surname")." ".$subject->getPII("firstname"); ?></td>
                <td><?php echo $subject->getPII("birthdate"); ?></td>
                <td><?php echo $subject->getPII("email"); ?></td>
                <td class="subject-comment"><?php echo $subject->getComment(); ?></td>
<?php

$previous_visit_date = null;
for ($i = 0; $i < NB_VISITS; $i ++){
    $visit_type = "V".($i + 1);
    $visit = null;
    foreach ($subject->getVisits() as $visit){
        if ($visit->getType() == $visit_type){
            break;
        }
    }
    if (! $visit){
        /* Should never happen. Log error */
        add_log("No visit of type ".$visit_type." found for subject ".$subject->getId());
?>
                <td class="subject-visit">
                </td>
<?php
        continue;
    }
    $visit_info = $visit_date_displ = "";
    $css = $visit->getStatusCSS();
    $visit_date = $visit->getDate();
    if ($visit_date){
        /* Date not null=appointment not null */
        $visit_info .= non_breakable_spaces("<ul><li>Date : "
                                            .$visit_date->format(DATE_FORMAT_UI)
                                            ."</li><li>Investigateur : "
                                            .$visit->getAppointment()->getInvestigatorName()
                                            ."</li></ul>");
        $visit_date_displ = $visit_date->format(DATE_FORMAT_UI_SHORT);
    }
    $survey = $visit->getSurvey();
    if (! $survey){
        /* Should never happen. Log error */
        add_log("No survey found for visit ".$visit_type." of subject ".$subject->getId());
        $survey_info = "";
    }
    else {
        $survey_info = "<ul><li>Token&nbsp;:&nbsp;".$survey->getToken()."</li><li>"
                     ."État : ".$survey->getStatusDescription()."</li><li>"
                     ."Début : ";
        if ($survey->getStartDate()){
            $survey_info .= $survey->getStartDate()->format(DATE_FORMAT_UI);
        }
        $survey_info .= "</li><li>Fin : ";
        if ($survey->getCompletionDate()){
            $survey_info .= $survey->getCompletionDate()->format(DATE_FORMAT_UI);
        }
        $survey_info .= "</li></ul>";
        $survey_info = non_breakable_spaces($survey_info);
    }
?>
                <td class="subject-visit">
                    <details data-popover="bottom">
                        <summary class="subject-visit-date <?php echo $css['text']; ?>">
                            <span class="fa-solid <?php echo $css['icon']; ?>" title="<?php echo $visit->getStatusDescription(); ?>"></span>&nbsp;<?php echo $visit_date_displ; ?>
<?php
    if ($previous_visit_date){
        $min_date = clone($previous_visit_date);
        $min_date->add(new DateInterval(MIN_TIME_TWO_VISITS));
        $max_date = clone($previous_visit_date);
        $max_date->add(new DateInterval(MAX_TIME_TWO_VISITS));
        if ($visit_date){
            if ($visit_date < $min_date or $visit_date > $max_date){
?>
                            <span class="fa-solid fa-clock icon-alert-info" title="Date de visite hors limite (<?php echo $visit_date->diff($previous_visit_date)->format("%a"); ?> jours depuis la visite précédente)"></span>
<?php

            }
        }
        else {
            $now = new DateTime();
            $message = null;
            if ($now > $max_date){
                $message = "Retard de ".$now->diff($max_date)->format("%a")
                         ." jours après la date maximale";
            }
            elseif ($now > $min_date){
                $message = "Pas de date de visite encore fixée";
            }
            if ($message){
?>
                           <span class="fa-solid fa-clock icon-alert-warning" title="<?php echo $message; ?>"></span>
<?php
            }
        }
    }
    $previous_visit_date = $visit_date;
?>
                        </summary>
<?php
    if ($visit_info){
?>
                        <div class="subject-visit-info">
                            <?php echo $visit_info; ?>
                        </div>
<?php
    }
?>
                    </details>
                    <details data-popover="bottom">
                        <summary class="display-subject-visit-survey-info">
                            <span class="fa-solid fa-circle-plus" title="Afficher les info du questionnaire"></span>&nbsp;Questionnaire
<?php
    if ($visit_date){
        $max_date = clone($visit_date);
        $max_date->add(new DateInterval(MAX_TIME_VISIT_SURVEY_COMPLETION_ACCEPT));
        if ($survey and $survey->getCompletionDate()){
            if ($survey and $survey->getCompletionDate() > $max_date){
?>
                            <span class="fa-solid fa-clock icon-alert-info" title="Délai de réponse hors limite (<?php echo round(($survey->getCompletionDate()->format("U") - $visit_date->format("U"))/3600); ?> h après la visite)"></span>
<?php
            }
        }
        elseif ($visit->getStatus() == Visit::STATUS_APPT_DONE){
            $now = new DateTime();
            if ($max_date < new DateTime()){
?>
                            <span class="fa-solid fa-clock icon-alert-warning" title="En attente. Retard de <?php echo round(($now->format("U") - $max_date->format("U"))/3600); ?> h"></span>
<?php
            }
        }
    }
?>
                        </summary>
                        <div class="subject-visit-survey-info">
                            <?php echo $survey_info; ?>
                        </div>
                    </details>
                </td>
<?php
    $subject->deleteVisit($visit);
}

$other_visits_info = "";
if ($subject->getVisits()){
    foreach ($subject->getVisits() as $visit){
        $css = $visit->getStatusCSS();
        $other_visits_info .= "<details data-popover=\"bottom\">"
                           ."<summary class=\"".$css["text"]."\">"
                           ."<span class = \"fa-solid ".$css["icon"]."\" title=\""
                           .$visit->getStatusDescription()."\"></span>&nbsp;"
                           /* Visit type "other" -> date can't be null */
                           .$visit->getDate()->format(DATE_FORMAT_UI_SHORT)
                           ."</summary>"
                           ."<div class=\"subject-other-visit-info\">"
                           .non_breakable_spaces("<ul><li>Type : "
                                                 .$visit->getTypeDescription()
                                                 ."</li><li>Date : "
                                                 .$visit->getDate()->format(DATE_FORMAT_UI)
                                                 ."</li><li>Investigateur : "
                                                 .$visit->getAppointment()->getInvestigatorName()
                                                 ."</li></ul>")
                           ."</div></details>";
    }
}
?>
                <td class="subject-other-visits">
                    <?php echo $other_visits_info; ?>
                </td>


                <td class="subject-alerts">
<?php
if ($subject->getAlerts()){
        $alerts_info = "<ul>";
        foreach($subject->getAlerts() as $alert){
            $css = $alert->getLevelCSS();
            $alerts_info .= "<li class=\"alert ".$css["text"]."\"><strong><span class=\"fa-solid "
                         .$css["icon"]."\" title=\"".$alert->getLevelDescription()."\"></span> "
                         .$alert->getDate()->format(DATE_FORMAT_UI)
                         ." | ".$alert->getType()
                         ."</strong> : ".$alert->getDescription();
            if ($_SESSION["rights"]["get_PII"]){
                if ($alert->getMessageAdmin()){
                    $alerts_info .= "<br/><strong>Message admin : </strong>".$alert->getMessageAdmin();
                }
                if ($alert->getMessageSubject()){
                    $alerts_info .= "<br/><strong>Message sujet : </strong>".$alert->getMessageSubject();
                }
            }
            $alerts_info .= "</li>\n";
        }
        $alerts_info .= "</ul>";
?>
                    <details data-popover="left">
                        <summary class="display-alerts-info">
                              <span class="fa-solid fa-circle-plus" title="Afficher les alertes"></span>&nbsp;détail
                        </summary>
                        <div class="alerts-info">
                            <?php echo $alerts_info; ?>
                        </div>
                    </details>
<?php
}
?>
                </td>
            </tr>
<?php
}
?>

        </tbody>
    </table>
</div>



<!--
-------------------
Orphan appointments
-------------------
-->

<h2>Rendez-vous orphelins</h2>
<p>
    Rendez-vous pas (encore) attachés à un sujet.
</p>
<?php
if ($dm["appointments"]){
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sm">
        <thead class="table-dark">
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Identité<br/>Nom Prénom</th>
                <th scope="col">Courriel</th>
                <th scope="col">Commentaires</th>
                <th scope="col">Statut</th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach ($dm["appointments"] as $appointment){
?>
            <tr>
                <td><?php echo $appointment->getDate()->format(DATE_FORMAT_UI); ?></td>
                <td><?php echo $appointment->getPII("subject_surname")." ".$appointment->getPII("subject_firstname"); ?></td>
                <td><?php echo $appointment->getPII("subject_email"); ?></td>
                <td><?php echo $appointment->getPII("comment"); ?></td>
                <td><?php echo $appointment->getStatusDescription(); ?></td>
            </tr>
<?php
    }
?>
        </tbody>
    </table>
</div>
<?php
}
else {
?>
    <p class="alert alert-success">
        aucun
    </p>
<?php
}




if ($dm["surveys"]){
?>
<h2>Questionnaires orphelins</h2>
<p>
    Questionnaires non attachés à un sujet
</p>
<p class="alert alert-danger">
    Il s'agit d'une anomalie.
</p>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="table-dark">
            <tr>
                <th scope="col">Token</th>
                <th scope="col">Date de début</th>
                <th scope="col">Date de fin</th>
                <th scope="col">Statut</th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach ($dm["surveys"] as $survey){
        $start_date = "";
        if ($survey->getStartDate()){
            $start_date = $survey->getStartDate()->format(DATE_FORMAT_UI);
        }
        $completion_date = "";
        if ($survey->getCompletionDate()){
            $completion_date = $survey->getCompletionDate()->format(DATE_FORMAT_UI);
        }
?>
            <tr>
                <td><?php echo $survey->getToken(); ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $completion_date; ?></td>
                <td><?php echo $survey->getStatusDescription(); ?></td>
            </tr>
<?php
    }
?>
        </tbody>
    </table>
</div>
<?php
}
?>


<h2>Alertes</h2>

<p>
    Alertes générées lors du <i>data management</i>.
</p>


<?php
if ($dm["alerts"]){
?>
<p>
    Nombre = <?php echo count($dm["alerts"]); ?>
</p>
<?php
    foreach ($dm["alerts"] as $alert){
        $css = $alert->getLevelCSS();
?>
<div class="alert <?php echo $css["text"]; ?>">
    <strong>
    <span class="fa-solid <?php echo $css["icon"]; ?>" title="<?php echo $alert->getLevelDescription(); ?>"></span>
    <?php echo $alert->getDate()->format(DATE_FORMAT_UI); ?> |
    Code = <?php echo $alert->getType(); ?> :
    </strong>
    <?php echo $alert->getDescription(); ?>
<?php
        if ($_SESSION["rights"]["get_PII"]){
?>
    <dl>
<?php
            if ($alert->getMessageAdmin()){
?>
        <dt>Message aux administrateurs :</dt><dd><?php echo $alert->getMessageAdmin(); ?></dd>
<?php
            }
            if ($alert->getMessageSubject()){
?>
        <dt>Message au sujet :</dt><dd><?php echo $alert->getMessageSubject(); ?></dd>
<?php
            }
?>
    </dl>
<?php
        }
?>
</div>
<?php
    }
}
else {
?>
<p class="alert alert-success">
    Aucune
</p>
<?php
}
?>



<script type="text/javascript">
/* const el_display_survey_info = document.querySelectorAll(".display-survey-info"); */
/* /\* console.log(el_display_survey_info); *\/ */
/* el_display_survey_info.forEach(function(el){ */
/*     el.addEventListener("click", function(ev){ */
/*         const el_survey_info = ev.currentTarget.parentNode.querySelector(".survey-info"); */
/*         if (el_survey_info.style.display == "block"){ */
/*             el_survey_info.style.display = "none"; */
/*         } else { */
/*             el_survey_info.style.display = "block"; */
/*         } */
/*     }) */
/* }); */
const el_toggle_display_subjects = document.getElementById("toggle-display-subjects");
const el_toggle_display_subjects_icon = document.getElementById("toggle-display-subjects-icon");
const el_toggle_display_subjects_label = document.getElementById("toggle-display-subjects-label");
toggle_display_subjects = function(action = "show"){
    if (action == "show"){
        el_toggle_display_subjects.value = 1;
        el_toggle_display_subjects_icon.classList.remove("fa-eye");
        el_toggle_display_subjects_icon.classList.add("fa-eye-slash");
        el_toggle_display_subjects_label.textContent = "Masquer les sujets non inclus";
    }
    else {
        el_toggle_display_subjects.value = 0;
        el_toggle_display_subjects_icon.classList.remove("fa-eye-slash");
        el_toggle_display_subjects_icon.classList.add("fa-eye");
        el_toggle_display_subjects_label.textContent = "Afficher les sujets non inclus";
    }
    const el_subject_rows = document.querySelectorAll(".subject-not-included");
    const el_subject_rows_l = el_subject_rows.length;
    let i;
    for (i = 0; i < el_subject_rows_l; i ++) {
        if (action == "show"){
            el_subject_rows[i].style.display = "";
        }
        else {
            el_subject_rows[i].style.display = "none";
        }
    };
}

el_toggle_display_subjects.addEventListener("click", function(){
    if (el_toggle_display_subjects.value == 1){
        toggle_display_subjects("hide");
    }
    else {
        toggle_display_subjects("show");
    }
});
toggle_display_subjects("hide");

const el_subject_numbers = document.querySelectorAll(".subject-number");
el_subject_numbers.forEach(function(el){
    el.addEventListener("dblclick", function(ev){
        el_row = ev.currentTarget.parentNode;
        if (el_row.classList.contains("subject-highlighted")){
            el_row.classList.remove("subject-highlighted");
        }
        else {
            el_row.classList.add("subject-highlighted");
        }
    })
});
</script>

<?php
include("footer.html");
