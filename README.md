
Helper program to manage research information in a prospective study using self-assessment questionaire with [LimeSurvey](https://www.limesurvey.org) and self-booking visits with [EasyAppointments](https://easyappointments.org/).

# Features

- data management script intended to run periodically (e.g. with a cron job)
- web dashboard

