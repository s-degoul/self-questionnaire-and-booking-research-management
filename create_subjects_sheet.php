<?php

include_once("config.php");
include_once("functions.php");

/* Delete previous subjects' sheets and QR code */
try {
    /* Subject sheets */
    $command_output = $command_result = null;
    exec("find ".DIR_DATA." -regex '".DIR_DATA.FILE_SUBJECT_SHEET_COMMON.".+$' -exec rm {} \;",
         $command_output, $command_result);
    if($command_result != 0){
        throw new Exception("Error in deletion of previous subjects' sheets: ".implode("\n", $command_output));
    }
    /* QR code */
    $command_output = $command_result = null;
    exec("find ".DIR_DATA."/img/*.png -type f -exec rm {} \;",
         $command_output, $command_result);
    if($command_result != 0){
        throw new Exception("Error in deletion of previous QR codes: ".implode("\n", $command_output));
    }
}
catch (Exception $e){
    die($e->getMessage());
}

/* Loading subjects */
$subjects = $alerts = array();
load_subjects_list($subjects, $alerts);

/* Loading files */
try {
    $model = @file_get_contents(FILE_SUBJECT_SHEET_MODEL);
    if ($model === false){
        throw new Exception("Unable to import subjects sheet model ".FILE_SUBJECT_SHEET_MODEL);
    }
}
catch (Exception $e){
    die($e->getMessage());
}


/* QR code of study website */
try {
    $command_output = $command_result = null;
    exec("qrencode -o ".DIR_DATA."img/".FILE_QR_STUDY_BASENAME.".png ".STUDY_URL, $command_output, $command_result);
    if($command_result != 0){
        throw new Exception("Error in QR code creation of study website: ".implode("\n", $command_output));
    }
}
catch (Exception $e){
    die($e->getMessage());
}


foreach ($subjects as $subject){
    $subject_id = $subject->getId();

    /* Create QR code of surveys */
    $visits_info = array();
    try {
        foreach ($subject->getVisits() as $visit){
            if (! $visit->getSurvey()){
                continue;
            }
            $QR_file_basename = $subject_id."_".$visit->getType();
            $survey_URL = $visit->getSurvey()->getURL();

            $command_output = $command_result = null;
            exec("qrencode -o ".DIR_DATA."img/".$QR_file_basename.".png ".$survey_URL, $command_output, $command_result);
            if($command_result != 0){
                throw new Exception("Error in QR code creation of visit ".$visit->getType()." for subject ".$subject_id.": ".implode("\n", $command_output));
            }
            $visits_info[$visit->getType()]["QR_code"] = $QR_file_basename;
            $visits_info[$visit->getType()]["URL_survey"] = $survey_URL;
        }
    }
    catch (Exception $e){
        die($e->getMessage());
    }

    /* Create LaTeX content */
    /* FIXME: need to escape some characters for LaTeX compilation (such as "_", "\") */
    $subject_sheet = $model;
    $subject_sheet = str_replace("<URL_study>", STUDY_URL, $subject_sheet);
    $subject_sheet = str_replace("<QR_code_study>", FILE_QR_STUDY_BASENAME, $subject_sheet);
    $subject_sheet = str_replace("<number>", $subject->getNumber(), $subject_sheet);
    $subject_sheet = str_replace("<id>", $subject_id, $subject_sheet);

    $group_info = explode("|", $subject->getGroup());
    $subject_sheet = str_replace("<treatment_group>", $group_info[0], $subject_sheet);
    $subject_sheet = str_replace("<ward_group>", $group_info[1], $subject_sheet);

    $instructions_V1 = "";
    if ($group_info[0] == "aroma-contrôle"){
        $treatment_V1_V2 = "aromathérapie";
        $treatment_V2_V3 = "aucun";
        $instructions_V2 = "\n\\vspace{1ex}\n\\raggedright\\myemph{Rapportez les sticks d'aromathérapie}";
        $instructions_V3 = "";
    }
    else if ($group_info[0] == "contrôle-aroma") {
        $treatment_V1_V2 = "aucun";
        $treatment_V2_V3 = "aromathérapie";
        $instructions_V2 = "";
        $instructions_V3 = "\n\\vspace{1ex}\n\\raggedright\\myemph{Rapportez les sticks d'aromathérapie}";
    }
    else {
        die("Unknown group ".$group_info[0]);
    }
    $subject_sheet = str_replace("<treatment_V1_V2>", $treatment_V1_V2, $subject_sheet);
    $subject_sheet = str_replace("<treatment_V2_V3>", $treatment_V2_V3, $subject_sheet);
    $subject_sheet = str_replace("<instructions_V1>", $instructions_V1, $subject_sheet);
    $subject_sheet = str_replace("<instructions_V2>", $instructions_V2, $subject_sheet);
    $subject_sheet = str_replace("<instructions_V3>", $instructions_V3, $subject_sheet);

    for ($i = 1; $i <= NB_VISITS; $i ++){
        $visit_type = "V".$i;
        if (! array_key_exists($visit_type, $visits_info)){
            /* FIXME?: log */
            die("No QR code available for visit ".$visit_type." for subject ".$subject_id);
        }
        $subject_sheet = str_replace("<QR_code_".$visit_type.">", $visits_info[$visit_type]["QR_code"], $subject_sheet);
        $subject_sheet = str_replace("<URL_survey_".$visit_type.">", $visits_info[$visit_type]["URL_survey"], $subject_sheet);
    }


    /* DEBUG */
    /* print_output($subject_sheet); */

    try {
        $file_basename = FILE_SUBJECT_SHEET_COMMON.$subject_id;
        $latex_file = DIR_DATA.$file_basename.".tex";
        $f = @fopen($latex_file, "w");
        if ($f === false){
            throw new Exception("Unable to write ".$latex_file." file");
        }
        fwrite($f, $subject_sheet);
        fclose($f);

        /* Compile to pdf and delete LaTeX file and compilation files */
        $command_output = $command_result = null;
        exec("pdflatex -halt-on-error -output-directory ".DIR_DATA." ".$latex_file." && find ".DIR_DATA." -regex '".DIR_DATA.$file_basename."\..+$' -and ! -regex '".DIR_DATA.".*\.pdf$' -exec rm {} \;",
             $command_output, $command_result);
        if($command_result != 0){
            throw new Exception("Error in LaTeX compilation of subject sheet ".$file_basename.": ".implode("\n", $command_output));
        }
    }
    catch (Exception $e){
        die($e->getMessage());
    }
}

/* Concatenate subject sheets */
try {
    $files_subjects_sheets = array();
    foreach ($subjects as $subject){
        $files_subjects_sheets[] = DIR_DATA.FILE_SUBJECT_SHEET_COMMON.$subject->getId().".pdf";
    }
    $command_output = $command_result = null;
    exec("pdftk ".implode(" ", $files_subjects_sheets)." cat output ".FILE_SUBJECTS_SHEETS);
    if($command_result != 0){
        throw new Exception("Error in concatenation of subjects' sheets in one file: ".implode("\n", $command_output));
    }

}
catch (Exception $e){
    die($e->getMessage());
}
