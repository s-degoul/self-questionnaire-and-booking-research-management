<?php
if(! defined("CONFIG_LOADED")) {
    http_response_code(403);
    die("Forbidden");
}

/**
 * Load dependencies
 */
include_once("vendor/autoload.php");


/**
 * Class auto loading
 */
spl_autoload_register(function ($class) {
    $class_file = "classes/".$class.".php";
    if (! file_exists($class_file)){
        throw new Exception("Unable to load class ".$class);
    }
    include_once($class_file);
});


/**
 * Print variable for debug purpose
 * @param   mixed   $a
 * @return  none
 */
function print_output($a){
    if (MODE != "dev"){
        return;
    }
    if (is_array($a) or is_object($a)){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }
    else {
        echo $a."<br/>";
    }
}


/**
 * Register a log in FILE_LOG file
 * @param   string  $log_message    Information to log
 * @param   string  $log_level      Log level, among ERROR, WARNING, NOTICE
 * @return  int
 */
function add_log(string $log_message, string $log_level = "ERROR"){
    if (! in_array($log_level, array("ERROR", "WARNING", "NOTICE"))){
        $log_level = "unknown";
    }
    $f = @fopen(FILE_LOG, "a+");
    if (! $f){
        die("Unable to write in log file ".FILE_LOG);
    }
    $now = new DateTime();
    fwrite($f, "[".$log_level."] ".$now->format(DATE_FORMAT_LOG)." - ".$log_message.PHP_EOL);
    fclose($f);
    return 0;
}

/**
 * Get url of the application
 * @param   none
 * @return  string   URL
 */
function url(){
    if(isset($_SERVER["HTTPS"])){
        $protocol = ($_SERVER["HTTPS"] && $_SERVER["HTTPS"] != "off") ? "https" : "http";
    }
    else{
        $protocol = "http";
    }
    return $protocol . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
}


/**
 * Replace spaces with non-breakable spaces of a string
 * @param   string  $input  input string
 * @return  string          output string
 */
function non_breakable_spaces($input){
    return str_replace(" ", "&nbsp;", $input);
}



/*************************/
/* Data management files */
/*************************/


/**
 * Get all available data management backup file present in DIR_DATA named according to FILE_BACKUP_NAME_FORMAT config
 * @param   none
 * @return  array   $dm_files   List of filenames
 */
function get_dm_files(){
    $files = scandir(DIR_DATA);
    $dm_files = array();
    foreach ($files as $file){
        if (preg_match("#^".str_replace("{date}", "(.+)", FILE_BACKUP_NAME_FORMAT)."$#", $file, $result)){
            $dm_files[] = array(
                "file" => DIR_DATA.$file,
                "date" => date_create_from_format(DATE_FORMAT_FILE, $result[1])
            );
        }
    }
    return $dm_files;
}

/**
 * Get data management backup file at a given date
 * @param   string  $date  date of the backup, formated according to DATE_FORMAT_FILE config or "last"
 * @return  string  $dm_file    filename of backup file
 */
function get_dm_file(string $date){
    if ($date == "last"){
        $file_date = null;
        $dm_file = null;
        foreach(get_dm_files() as $file){
            if (is_null($file_date)
                or $file_date < $file["date"]){
                $file_date = $file["date"];
                $dm_file = $file["file"];
            }
        }
    }
    else {
        $dm_file = DIR_DATA.str_replace("{date}", $date, FILE_BACKUP_NAME_FORMAT);
        if (! file_exists($dm_file)){
            throw new Exception("File ".$dm_file." doesn't exist");
        }
    }
    return $dm_file;
}


/**
 * Get a setter method for an attribute
 * @param   string  $attr   Name of attribute
 * @param   string  $method_type    Type of method: "set" (attribute's value is an object or builtin type) or "add" (attribute's value is an array)
 * @param   string  $class  Class name
 * @return  string  $method Method name
 */
function get_method_from_attr(string $attr, string $method_type, string $class){
    $method = $method_type.str_replace("_", "", ucwords($attr, "_"));
    if ($method_type == "add"
        and substr($method, -1) == "s"){
        $method = substr($method, 0, -1);
    }
    if (! method_exists($class, $method)){
        throw new Exception("Unknown method ".$method." in class ".$class);
    }
    return $method;
}

/**
 * Get a class from attribute name of another class
 * @param   string  $attr   Attribute name
 * @param   boolean $plural Is attribute name in plural form? (with an trailing "s")
 * @return  string  $class  Class name
 */
function get_class_from_attr(string $attr, bool $plural = false){
    /* Special handling of some attributes of DateTime type
     * HINT: set in config? Compute automatically? */
    if (in_array($attr, array("date", "start_date", "end_date", "completion_date"))){
        return "DateTime";
    }
    $class = str_replace("_", "", ucwords($attr, "_"));
    if ($plural
        and substr($class, -1) == "s"){
        $class = substr($class, 0, -1);
    }
    if (! class_exists($class)){
        throw new Exception("Unknown class ".$class);
    }
    return $class;
}


/**
 * Convert an array to an object of a given class
 * @param   array   $input  Array of key/value with:
 * - key = class attribute or integer in case of array of objects
 * - value = value of attribute (can be an array)
 * @param   string  $class  Name of the class
 * @return  object  $obj    A object instance of class $class
 */
function array_to_object(array $input, string $class){
    if (! class_exists($class)){
        throw new Exception("Unknown class ".$class);
    }
    /* Special handling of DateTime object.
     * $input["date"] should contain string version of date ready to use by DateTime constructor */
    else if ($class == "DateTime"){
        return new DateTime($input["date"]);
    }
    $obj = new $class;
    foreach ($input as $key => $value){
        /* If no value, no setter function called */
        if (! $value){
            continue;
        }
        /* $value is array */
        if (is_array($value) and ! empty($value)){
            /* key is integer => $value is an array of objects
             * "add" method called for each object */
            if (is_integer(array_keys($value)[0])){
                $add_method = get_method_from_attr($key, "add", $class);
                foreach ($value as $val){
                    $obj->$add_method(array_to_object($val, get_class_from_attr($key, true)));
                }
            }
            /* key is an attribute name => $value is one object
             * "set" method called for this object */
            else {
                $set_method = get_method_from_attr($key, "set", $class);
                $obj->$set_method(array_to_object($value, get_class_from_attr($key)));
            }
        }
        /* $value is not an array => can be "set" to attribute as is */
        else {
            $set_method = get_method_from_attr($key, "set", $class);
            $obj->$set_method($value);
        }
    }
    return $obj;
}


/**
 * Load json-formated data from a previous data management
 * from a backup file
 * @param   string  $date   date of the backup, formated according to DATE_FORMAT_FILE config or "last"
 * @return  array   $result array of 4 arrays with Subject / Appointment / Survey / Alert objects, respectively
 */
function load_dm(string $date){
    $result = array(
        "subjects" => array(),
        "appointments" => array(),
        "surveys" => array(),
        "alerts" => array()
    );

    $dm = array();
    $dm_file = get_dm_file($date);
    if ($dm_file){
        $dm = json_decode(file_get_contents($dm_file), true);
    }
    if (empty($dm)){
        return $result;
    }

    foreach ($result as $data_type => $dummy){
        $class = get_class_from_attr($data_type, true);
        foreach($dm[$data_type] as $dm_object_data){
            $result[$data_type][] = array_to_object($dm_object_data, $class);
        }
    }

    return $result;
}


/**
 * Save data management result (subjects list) in a json format
 * in a backup file named according to FILE_BACKUP_NAME_FORMAT config
 * @param   array   $subjects       List of Subject objects
 * @param   array   $appointments   List of Appointment objects
 * @param   array   $surveys        List of Survey objects
 * @param   array   $alerts         List of Alert objects
 * @return  int
 */
function save_dm(array $subjects, array $appointments, array $surveys, array $alerts){
    $dm = array(
        "subjects" => $subjects,
        "appointments" => $appointments,
        "surveys" => $surveys,
        "alerts" => $alerts
    );
    $result = json_encode($dm);

    try {
        $dm_file = DIR_DATA.str_replace("{date}", date(DATE_FORMAT_FILE), FILE_BACKUP_NAME_FORMAT);
        $f = fopen($dm_file, "w");
        fwrite($f, $result);
        fclose($f);
    }
    catch (Exception  $e){
        add_log("Unable to save data management result in file ".$dm_file.": ".$e->getMessage());
        exit(1);
    }
    return 0;
}





/*****************/
/* Subjects list */
/*****************/

/**
 * Import subjects list CSV file content
 * @param   none
 * @return  array   $file_content
 */
function import_subjects_list_file(){
    /* Expected variables: relative to id, number and surveys' token */
    $expected_variables = array(SUBJECTS_LIST_VAR_ID, SUBJECTS_LIST_VAR_NUMBER, SUBJECTS_LIST_VAR_COMMENT);
    for ($i = 1; $i <= NB_VISITS; $i ++){
        $expected_variables[] = SUBJECTS_LIST_VAR_SURVEY_TOKEN."V".$i;
    }
    if (defined("SUBJECTS_LIST_VAR_GROUP")){
        $expected_variables[] = SUBJECTS_LIST_VAR_GROUP;
    }

    try {
        $f = fopen(FILE_SUBJECTS_LIST, "r");
        $variables = array();
        $file_content = array();

        $i = 0;
        $values_nb_first_line = 0;
        while ($line = fgetcsv($f, 1000, SEP_CSV)){
            $i ++;
            $values_nb = count($line);
            /* First line = variables' name */
            if ($i == 1){
                $values_nb_first_line = $values_nb;
                /* if ($values_nb < $expected_variables_nb){ */
                /*     throw new Exception("Some variables are missing (expected: ".implode(", ", $expected_variables).")"); */
                /* } */
                for($j = 0; $j < $values_nb; $j++){
                    $pos = array_keys($expected_variables, $line[$j]);
                    if (count($pos) != 1){
                        continue;
                    }
                    $variables[$j] = $line[$j];
                    unset($expected_variables[$pos[0]]);
                }
                if (count($expected_variables)){
                    throw new Exception("Some variables are missing: "
                                        .implode(", ", $expected_variables));
                }
                continue;
            }
            /* Other lines = values */
            if ($values_nb != $values_nb_first_line){
                throw new Exception("Line ".$i.": wrong number of values (expected ".$values_nb_first_line.")");
            }
            $file_content[] = array();
            for ($j = 0; $j < $values_nb; $j++){
                if (in_array($j, array_keys($variables))){
                    $file_content[count($file_content) - 1][$variables[$j]] = $line[$j];
                }
            }
        }
        fclose($f);
    }
    catch (Exception  $e){
        add_log("Unable to import subjects list file: ".$e->getMessage());
        exit(1);
    }

    return $file_content;
}

/**
 * Load subjects info from CSV subjects list and merging with previous data management
 * @param   array   $subjects   List of Subject objects
 * @param   array   $alerts     List of Alert objects
 * @return  int
 */
function load_subjects_list(array &$subjects, array &$alerts){
    /* File content and number of rows */
    $subjects_list_file_content = import_subjects_list_file();
    $subjects_list_file_content_nb = count($subjects_list_file_content);
    /* Subjects already exist from previous data management? */
    $subjects_exist_previous_dm = ! empty($subjects);

    /* Loop throw each CSV row */
    for ($i = 0; $i < $subjects_list_file_content_nb; $i++){
        /* Subject ID and number defined in SUBJECTS_LIST_VAR_ID
         * and SUBJECTS_LIST_VAR_NUMBER variables of CSV array */
        $subject_id = $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_ID];
        $subject_number = $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_NUMBER];

        /* Test if subject already exists in previous data management */
        $subject_exists = false;
        foreach ($subjects as $subject){
            if ($subject->getId() == $subject_id){
                $subject_exists = true;
                break;
            }
        }
        /* Creating new subject if not already exists */
        if (! $subject_exists){
            $subject = new Subject();
            $subject->setId($subject_id);
            $subject->setNumber($subject_number);
            /* Alert, only if subjects already exist from previous data management */
            if ($subjects_exist_previous_dm){
                $subject->createAlert(new Alert(
                    Alert::LEVEL_WARNING,
                    "new_subject_in_subjects_list",
                    "Un nouveau sujet est apparu : identifiant = ".$subject->getCompleteId()),
                                      $alerts);
            }
            /* FIXME: subject id in array key usefull? */
            $subjects[$subject->getId()] = $subject;

        }
        /* Warning if subject number has changed, but new value are applied FIXME:doc */
        elseif ($subject_exists
            and $subject->getNumber() != $subject_number){
            /* Alert */
            $subject->createAlert(new Alert(
                Alert::LEVEL_ERROR,
                "subject_number_changed",
                "Sujet ".$subject->getCompleteId()." : le n° d'anonymat a changé et est maintenant ".$subject_number),
                                  $alerts);
            $subject->setNumber($subject_number);
        }

        /* Creating/checking NB_VISITS visits with their corresponding survey */
        for ($j = 1; $j <= NB_VISITS; $j ++){
            /* Mandatory visits type = Vx */
            $visit_type = "V".$j;
            /* Survey token : CSV variable according to SUBJECTS_LIST_VAR_SURVEY_TOKEN config */
            $survey_token = $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_SURVEY_TOKEN."V".$j];
            /* Test if visit already exists
             * but don't change other visits for now (e.g. exceptional) */
            $visit_exists = false;
            if ($subject_exists) {
                foreach ($subject->getVisits() as $visit){
                    if ($visit->getType() == $visit_type){
                        $visit_exists = true;
                        break;
                    }
                }
            }
            /* Creating new visit */
            if (! $visit_exists){
                /* Alert if expected visit not present in already existing subject */
                if ($subject_exists){
                    $subject->createAlert(new Alert(
                        Alert::LEVEL_ERROR,
                        "visit_mandatory_type_previously_absent",
                        "Sujet ".$subject->getCompleteId()." : aucune visite de type ".$visit_type." n'existait dans la version précédente du data management"),
                                          $alerts);
                }
                $visit = new Visit();
                $visit->setType($visit_type);
                $survey = new Survey();
                $visit->setSurvey($survey);
                $subject->addVisit($visit);
            }
            /* Alert if survey token has changed */
            else if ($visit->getSurvey()->getToken() != $survey_token){
                $subject->createAlert(new Alert(
                    Alert::LEVEL_ERROR,
                    "survey_token_changed",
                    "Sujet ".$subject->getCompleteId()." : le token du questionnaire de la visite ".$visit_type." était ".$visit->getSurvey()->getToken()." et est maintenant ".$survey_token),
                                      $alerts);
            }
            /* Apply survey token */
            $visit->getSurvey()->setToken($survey_token);
        }

        /* Adding comment */
        $subject->setComment($subjects_list_file_content[$i][SUBJECTS_LIST_VAR_COMMENT]);
        /* Adding group */
        if (defined("SUBJECTS_LIST_VAR_GROUP")){
            $subject->setGroup($subjects_list_file_content[$i][SUBJECTS_LIST_VAR_GROUP]);
        }
    }

    /* Search for previously existing subjects which are no more present in the subjects list
     * and alert if so, but remove this subject FIXME:doc */
    foreach ($subjects as $key => $subject){
        $subject_exists = false;
        for ($i = 0; $i < $subjects_list_file_content_nb; $i++){
            if ($subject->getId() == $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_ID]){
                $subject_exists = true;
            }
        }
        if (! $subject_exists){
            $alerts[] = new Alert(
                Alert::LEVEL_ERROR,
                "subject_deleted",
                "Le sujet ".$subject->getCompleteId()." n'existe plus dans la liste des sujets");
            unset($subjects[$key]);
        }
    }

    return 0;
}




/*************************/
/* EasyAppointments data */
/*************************/

/**
 * Test access to EasyAppointments API with given credentials
 * @param   string   $user    username
 * @param   string   $password  password
 * @return  boolean
 */
function test_EA_API($username, $password){
    $auth = base64_encode($username.":".$password);
    $context = stream_context_create(array(
        "http" => array(
            "method" => "GET",
            "header" => "Authorization: Basic $auth",
            "ignore_errors" => false
        )
    ));
    if (@fopen(EA_API_URL."/appointments", "r", false, $context) !== false){
        return true;
    }
    return false;
}


/**
 * Import EasyAppointments data through API
 * @param   none
 * @return  array   $result Array with 2 nested arrays: appointments and customers information
 */
function import_EA_data(){
    $result = array();

    try {
        $auth = base64_encode(EA_API_USER.":".EA_API_PASSWORD);
        $context = stream_context_create(array(
            "http" => array(
                "method" => "GET",
                "header" => "Authorization: Basic $auth",
                "ignore_errors" => true
            )
        ));
        $result["appointments"] = json_decode(file_get_contents(EA_API_URL."/appointments", false, $context), true);
        $result["customers"] = json_decode(file_get_contents(EA_API_URL."/customers", false, $context), true);
        $result["providers"] = json_decode(file_get_contents(EA_API_URL."/providers", false, $context), true);
    }
    catch (Exception  $e){
        add_log("Unable to import appointments with API: ".$e->getMessage());
        exit(1);
    }

    return $result;
}

/**
 * Make a list of Appointment objects from EasyAppointments data
 * @param   none
 * @return  array   $appointments   List of Appointment objects
 */
function load_appointments(){
    $appointments = array();

    $EA_data = import_EA_data();

    try{
        foreach ($EA_data["appointments"] as $appointment_data){
            $appointment = new Appointment();

            $appointment->setId($appointment_data["id"]);
            $appointment->setDate(new DateTime($appointment_data["start"]));
            $appointment->setEndDate(new DateTime($appointment_data["end"]));
            $appointment->setComment($appointment_data["notes"]);
            /* Comment may contain information to set some attributes */
            $appointment->setInfoFromComment();

            /* Look for related customer */
            $customer_id = $appointment_data["customerId"];
            foreach ($EA_data["customers"] as $customer_data){
                if ($customer_data["id"] == $customer_id){
                    $appointment->setSubjectSurname($customer_data["lastName"]);
                    $appointment->setSubjectFirstname($customer_data["firstName"]);
                    $appointment->setSubjectEmail($customer_data["email"]);
                    $appointment->setSubjectBirthdate($customer_data["notes"]);
                    break;
                }
            }
            if (! $appointment->getSubjectEmail()){
                throw new Exception("No customer found for appointment id=".$appointment->getId());
            }
            foreach ($EA_data["providers"] as $provider_data){
                if ($provider_data["id"] == $appointment_data["providerId"]){
                    $appointment->setInvestigatorId($appointment_data["providerId"]);
                    $appointment->setInvestigatorName($provider_data["firstName"]." ".$provider_data["lastName"]);
                    break;
                }
            }

            $appointments[] = $appointment;
        }
    }
    catch (Exception $e){
        add_log("Error in appointments loading: ".$e->getMessage());
        exit(1);
    }

    return $appointments;
}



/*******************/
/* LimeSurvey data */
/*******************/


/**
 * Import LimeSurvey data through API
 * @param   none
 * @return  array   $result["responses"]    List of "control" information about responses (dates, token...)
 */
function import_LS_data(){
    try {
        /* Using a RPC client */
        $myJSONRPCClient = new \org\jsonrpcphp\JsonRPCClient(LS_API_URL);
        $sessionKey= $myJSONRPCClient->get_session_key(LS_API_USER, LS_API_PASSWORD);
        $items = array("id", "token", "startdate", "submitdate");
        /* Adding question about subject initials, if configured /!\ must be the last value of $items array */
        if (defined("LS_Q_SUBJECT_NAME_INITIALS")){
            $items[] = LS_Q_SUBJECT_NAME_INITIALS;
        }
        $result = $myJSONRPCClient->export_responses($sessionKey, LS_SURVEY_ID, "json", null, "all", "code", "long", null, null, $items);
        $myJSONRPCClient->release_session_key($sessionKey);

        /* No answers => return null */
        if (is_array($result)){
            return null;
        }

        $result = json_decode(base64_decode($result), true);

        /* Data in one nested array whose key is "responses" */
        return $result["responses"];
    }
    catch (Exception  $e){
        add_log("Unable to import surveys responses with API: ".$e->getMessage());
        exit(1);
    }
}

/**
 * Make a list of Survey objects from LimeSurvey data
 * @param   none
 * @return  array   $surveys    List of Survey objects
 */
function load_surveys(){
    $surveys = array();

    $surveys_data = import_LS_data();

    if ($surveys_data){
        try {
            foreach ($surveys_data as $survey_data){
                if (! is_array($survey_data) or count($survey_data) != 1){
                    throw new Exception("Survey data badly formated: ".serialize($survey_data));
                }
                /* Data present in one nested array whose key = survey's ID */
                $survey_data = reset($survey_data);

                $survey = new Survey();

                $survey->setId($survey_data["id"]);

                /* Missing token means incorrect LimeSurvey configuration */
                if (! $survey_data["token"]){
                    throw new Exception("No token for survey id=".$survey->getId());
                }
                $survey->setToken($survey_data["token"]);

                if ($survey_data["startdate"]){
                    $survey->setStartDate(new DateTime($survey_data["startdate"]));
                }

                if ($survey_data["submitdate"]){
                    $survey->setCompletionDate(new DateTime($survey_data["submitdate"]));
                }
                /* Complete status attribute from dates */
                $survey->setStatusFromDates();

                if (defined("LS_Q_SUBJECT_NAME_INITIALS")){
                    $survey->setSubjectNameInitials(end($survey_data));
                }

                $surveys[] = $survey;
            }
        }
        catch (Exception $e){
            add_log("Error in surveys loading: ".$e->getMessage());
            exit(1);
        }
    }

    return $surveys;
}




/**********************************/
/* Multiple sources data handling */
/**********************************/

/**
 * Sort list of objects by date parameter
 * @param   string  $method     class method to get date parameter value
 * @return  int                 0, 1 or -1 as expected by usort()
 */
function sort_objects_by_date($method){
    return function($object1, $object2) use ($method) {
        $object1_date = $object1->$method();
        $object2_date = $object2->$method();
        if ($object1_date == $object2_date){
            return 0;
        }
        return ($object1_date < $object2_date) ? -1 : 1;
    };
}

/**
 * Add appointments information to a subject
 * @param   array   $appointments   List of Appointment objects
 * @param   Subject $subject        A Subject object
 * @param   array   $alerts         List of Alert objects
 * @return  array   $subject_appointments   List of Appointment objects
 */
function add_appointments_subject(array &$appointments, Subject &$subject, array &$alerts){
    /* Get list of appointments for the subject, ordered by date */
    /* - based on explicit ID */
    $subject_appointments = array();
    foreach ($appointments as $key => $appointment){
        if ($appointment->getSubjectId() == $subject->getId()){
            $subject_appointments[] = $appointment;
            unset ($appointments[$key]);
        }
    }
    if (empty($subject_appointments)){
        return $subject_appointments;
    }

    /* - based on email, for appointments without subject ID */
    $subject_apptmt_emails = array_map(function($apptmt){
        return $apptmt->getSubjectEmail();
    }, $subject_appointments);
    foreach ($appointments as $key => $appointment){
        if (! $appointment->getSubjectId()
            and in_array($appointment->getSubjectEmail(), $subject_apptmt_emails)){
            $subject_appointments[] = $appointment;
            unset ($appointments[$key]);
        }
    }
    /* Sort appointments by date, increasing order */
    usort($subject_appointments, sort_objects_by_date("getDate"));

    /* Remove existing visits with type TYPE_UNKNOWN */
    foreach ($subject->getVisits() as $visit){
        if ($visit->getType() == Visit::TYPE_UNKNOWN){
            $subject->deleteVisit($visit);
        }
    }

    /* Alert if exceptional visit present in previous data doesn't exist anymore */
    foreach ($subject->getVisits() as $visit){
        if ($visit->getType() == Visit::TYPE_EXCEPTIONAL){
            $visit_exist_in_appointments = false;
            foreach ($subject_appointments as $appointment){
                if ($appointment->getVisitInfo() == Appointment::VISIT_INFO_EXCEPTIONAL
                    and $appointment->getDate() == $visit->getDate()){
                    $visit_exist_in_appointments = true;
                }
            }
            if (! $visit_exist_in_appointments){
                $subject->createAlert(new Alert(
                    Alert::LEVEL_WARNING,
                    "visit_exceptional_disappeared",
                    "Sujet ".$subject->getCompleteId()." : la visite ".$visit->getPublicDescription()." n'existe plus"),
                                      $alerts);
                $subject->deleteVisit($visit);
            }
        }
    }

    /* Complete visits information from appointments */
    /* Tracking number of normal visits */
    $visit_nb = 0;
    foreach ($subject_appointments as $appointment){
        /* Particular visit handling */
        /* Type unknown if: status of appointment is STATUS_UNKNOWN or $visit_nb > NB_VISITS (FIXME: doc) */
        if ($appointment->getStatus() == Appointment::STATUS_UNKNOWN
            or $visit_nb > NB_VISITS){
            $visit = new Visit();
            $visit->setType(Visit::TYPE_UNKNOWN);
            $visit->setAppointment($appointment);
            $subject->addVisit($visit);
            /* Create an alert */
            $subject->createAlert(new Alert(
                Alert::LEVEL_WARNING,
                "visit_type_unknown",
                "Sujet ".$subject->getCompleteId()." : visite de type ".$visit->getTypeDescription()." le ".$appointment->getDate()->format(DATE_FORMAT_UI)),
                                  $alerts);

            continue;
        }

        /* Type: exceptional */
        if ($appointment->getVisitInfo() == Appointment::VISIT_INFO_EXCEPTIONAL){
             $create_new_visit = true;
             /* Test if this visit already exists, i.e. same date (no matter if end_date has changed) */
             foreach ($subject->getVisits() as $visit){
                 if ($visit->getType() == Visit::TYPE_EXCEPTIONAL
                     and $visit->getDate() == $appointment->getDate()){
                     $create_new_visit = false;
                     break;
                 }
             }
             /* If this visit doesn't exist, create it */
             if ($create_new_visit){
                 $visit = new Visit();
                 $visit->setType(Visit::TYPE_EXCEPTIONAL);
                 $subject->addVisit($visit);
             }
             /* Set new version of appointment */
             $visit->setAppointment($appointment);

             continue;
        }

        /* Else, "normal" visit (type "Vx"): overwrite preexisting appointment info, if any */
        $visit_nb ++;
        foreach ($subject->getVisits() as $visit){
            if ($visit->getType() == "V".$visit_nb){
                $visit->setAppointment($appointment);
            }
        }
    }

    return $subject_appointments;
}


/**
 * Add surveys information to a subject
 * @param   array   $surveys    List of Survey objects
 * @param   Subject $subject    A Subject object
 * @param   array   $alerts     List of Alert objects
 * @return  int
 */
function add_surveys_subject(array &$surveys, Subject &$subject, array &$alerts){
    foreach ($subject->getVisits() as $visit){
        if ($visit->getSurvey()){
            $survey_token = $visit->getSurvey()->getToken();
            $survey_added = false;
            /* Add survey data, if exists */
            foreach ($surveys as $key => $survey){
                if ($survey->getToken() == $survey_token){
                    /* HINT: test if something has changed would be interesting, for further functionality? */
                    $visit->setSurvey($survey);
                    unset($surveys[$key]);
                    $survey_added = true;
                    break;
                }
            }
            /* Remove data from previously loaded survey, which doesn't exist anymore */
            if (! $survey_added){
                $survey = new Survey();
                $survey->setToken($survey_token);
                $visit->setSurvey($survey);
            }
        }
    }

    return 0;
}


/**
 * Test if some ID information are the same, besides orthographic variants (accent, font case, ...)
 * @param   array   $id_values  List of values
 * @param   bool    $rm_empty   Should empty values be excluded for comparison?
 * @return  bool    true if test succeded, false if not
 */
function check_id_info_unique(array $id_values, $rm_empty = false){
    $formated_id_values = array_unique(array_map(function($id_value){
        return trim(strtolower(Transliterator::create('NFD; [:Nonspacing Mark:] Remove; NFC')->transliterate($id_value)));
    }, $id_values));
    if ($rm_empty){
        foreach ($formated_id_values as $key => $value){
            if (! $value){
                unset($formated_id_values[$key]);
            }
        }
    }
    if (count($formated_id_values) > 1){
        return false;
    }
    return true;
}


/**
 * Complete subjects information with appointments and surveys lists
 * @param   array   $subjects       List of Subject objects
 * @param   array   $appointments   List of Appointment objects
 * @param   array   $surveys        List of Survey objects
 * @param   array   $alerts         List of Alert objects
 * @return  int
 */
function complete_subjects_info(array &$subjects, array &$appointments, array &$surveys, array &$alerts){
    foreach ($subjects as $subject){
        $subject_appointments = add_appointments_subject($appointments, $subject, $alerts);
        add_surveys_subject($surveys, $subject, $alerts);

        /* Complete ID information */
        if ($subject_appointments){
            /* DEBUG */
            /* print_output("<h2>".$subject->getCompleteId()."</h2>"); */

            /* Look for ID information values in appointments */
            foreach (array("surname", "firstname", "email", "birthdate") as $id_info){
                $apptmt_infos = array_map(function($apptmt) use ($id_info){
                    $get_subject_info_method = "getSubject".ucfirst($id_info);
                    return $apptmt->$get_subject_info_method();
                }, $subject_appointments);
                /* DEBUG */
                /* print_output($id_info); */
                /* print_output($apptmt_infos); */

                /* Only birthdate can be missing in valid appointment information */
                /* HINT: set in config? Compute automatically? */
                $rm_empty = ($id_info == "birthdate") ? true : false;
                if (! check_id_info_unique($apptmt_infos, $rm_empty)){
                    /* Alert */
                    $subject->createAlert(new Alert(
                        Alert::LEVEL_ERROR,
                        "subject_multiple_ID_values_appointments",
                        "Sujet ".$subject->getCompleteId()." : valeurs multiples de l'information \"".$id_info."\" dans les données des rendez-vous"),
                                          $alerts);
                }
                /* Select first non null value */
                $apptmt_info = $apptmt_infos[0];
                $i = 0;
                while ($i < count($apptmt_infos)){
                    if ($apptmt_infos[$i]){
                        $apptmt_info = $apptmt_infos[$i];
                        break;
                    }
                    $i ++;
                }
                /* Set value in subject */
                $get_method = "get".ucfirst($id_info);
                $set_method = "set".ucfirst($id_info);
                $subject_info = $subject->$get_method();
                if (! $subject_info){
                    /* No alert, ignore */
                    /* print_output($subject->getCompleteId()." - ID info ".$id_info.": no value"); */
                } else {
                    /* Alert if ID information has changed */
                    if (! check_id_info_unique(array($subject_info, $apptmt_info), $rm_empty)){
                        $subject->createAlert(new Alert(
                            Alert::LEVEL_ERROR,
                            "subject_ID_value_changed",
                            "Sujet ".$subject->getCompleteId()." : l'information \"".$id_info."\" est différente entre le data management précédent et celles des rendez-vous",
                            "Sujet ".$subject->getCompleteId()." : l'information \"".$id_info."\" est différente entre le data management précédent et celles des rendez-vous ; ancienne valeur = ".$subject_info.", nouvelle valeur = ".$apptmt_info),
                                              $alerts);
                    }
                }
                $subject->$set_method($apptmt_info);
            }
        }

        /* Complete visits information */
        /* Sort visit by date to compute time between two "normal" visits */
        $visits = $subject->getVisits();
        usort($visits, sort_objects_by_date("getType"));
        $subject->setVisits($visits);
        $previous_visit_date = null;
        foreach ($subject->getVisits() as $visit){
            /* Visit status */

            /* No appointment = visit not scheduled */
            if (! $visit->getAppointment()){
                $visit->setStatus(Visit::STATUS_NOT_SCHEDULED);
                /* Alert according to time since last visit */
                if ($previous_visit_date
                    and $visit->isNormal()){
                    $now = new DateTime();
                    $min_date = clone($previous_visit_date);
                    $min_date->add(new DateInterval(MIN_TIME_TWO_VISITS));
                    if ($now > $min_date){
                        $max_date = clone($previous_visit_date);
                        $max_date->add(new DateInterval(MAX_TIME_TWO_VISITS));
                        $subject->createAlert(new Alert(Alert::LEVEL_WARNING,
                                                     "visit_not_scheduled_yet",
                                                     "Sujet ".$subject->getCompleteId()." : la visite ".$visit->getTypeDescription()." n'est pas encore programmée (date admissible entre le "
                                                     .$min_date->format(DATE_FORMAT_UI_SHORT)." et le ".$max_date->format(DATE_FORMAT_UI_SHORT).")"/* , */
                                                     /* "", */
                                                     /* "Vous n'avez pas encore pris rendez-vous pour la visite ".$visit->getTypeDescription().", qui devrait avoir lieu entre le " */
                                                     /* .$min_date->format(DATE_FORMAT_UI_SHORT)." et le ".$max_date->format(DATE_FORMAT_UI_SHORT) */
                                                        ),
                                           $alerts);
                    }
                }
            }
            else {
                /* Keep old value of status and date for further check */
                $old_visit_status = $visit->getStatus();
                $old_visit_date = $visit->getDate();

                /* Visit date = appointment date */
                $visit->setDate($visit->getAppointment()->getDate());

                switch ($visit->getAppointment()->getStatus()){
                case Appointment::STATUS_SCHEDULED:
                    /* Appointment scheduled -> visit scheduled */
                    $visit->setStatus(Visit::STATUS_SCHEDULED);
                    break;
                case Appointment::STATUS_DONE:
                    /* Alert about new inclusion: no value for $old_visit_date and visit type = first visit */
                    if (! $old_visit_date
                        and $visit->getType() == "V1"){
                        $alerts[] = new Alert(
                            Alert::LEVEL_NOTICE,
                            "new_inclusion",
                            "Nouvelle inclusion",
                            "Nouvelle inclusion dans l'étude HERBAS du participant n° 1192-".$subject->getNumber()
                            ." (date de naissance : ".$subject->getBirthdate()
                            .") par l'investigateur ".$visit->getAppointment()->getInvestigatorName()
                            ." le ".$visit->getDate()->format(DATE_FORMAT_UI_SHORT),
                            null,
                            null,
                            EMAILS_ALERTS_INCLUSION_TO);
                    }
                    if ($visit->getSurvey()){
                        /* Appointment done and survey completed -> visit completed */
                        if ($visit->getSurvey()->getStatus() == Survey::STATUS_COMPLETED){
                            $visit->setStatus(Visit::STATUS_COMPLETED);
                            break;
                        }
                        /* Appointment done but survey not completed */
                        /* Appointment's visit info = "no survey" -> visit completed without survey */
                        if ($visit->getAppointment()->getVisitInfo() == Appointment::VISIT_INFO_NO_SURVEY){
                            $visit->setStatus(Visit::STATUS_COMPLETED_WO_SURVEY);
                            break;
                        }
                        /* Else, visit not completed, just "appointment done" */
                        $visit->setStatus(Visit::STATUS_APPT_DONE);
                        /* Alert if waiting for survey completion for > MAX_TIME_VISIT_SURVEY_COMPLETION_ALERT */
                        $max_date = clone($visit->getDate());
                        $max_date->add(new DateInterval(MAX_TIME_VISIT_SURVEY_COMPLETION_ALERT));
                        if ($max_date < new DateTime()){
                            $subject->createAlert(new Alert(
                                Alert::LEVEL_WARNING,
                                "visit_survey_completion_expected",
                                "Sujet ".$subject->getCompleteId()." : le questionnaire de la visite ".$visit->getPublicDescription()." n'a pas été encore complété",
                                null,
                                "Vous avez réalisé la visite ".$visit->getPublicDescription().". Nous avons remarqué que vous n'avez pas encore complété entièrement le questionnaire en rapport : ".$visit->getSurvey()->getURL()),
                                                  $alerts);
                        }
                    }
                    /* Appointment done and exceptional visit type -> visit completed */
                    else if ($visit->getType() == Visit::TYPE_EXCEPTIONAL){
                        $visit->setStatus(Visit::STATUS_COMPLETED);
                    }
                    break;
                case Appointment::STATUS_CANCELED:
                    /* Appointment canceled -> visit canceled */
                    $visit->setStatus(Visit::STATUS_CANCELED);
                    break;
                default:
                    /* FIXME: alert? Or STATUS_UNKNOWN -> nothing to do */
                }

                /* Compare with previous values */
                if ($visit->getStatus() < $old_visit_status){
                    /* Alert */
                    $subject->createAlert(new Alert(
                        Alert::LEVEL_WARNING,
                        "visit_inconsistent_status_change",
                        "Sujet ".$subject->getCompleteId()." : le statut de la visite ".$visit->getPublicDescription()." a changé d'une manière incohérente ; était \"".Visit::getPublicStatusDescription($old_visit_status)."\" et est maintenant \"".$visit->getStatusDescription()."\""),
                                          $alerts);
                }
                if ($old_visit_date
                    and $visit->getDate() != $old_visit_date){
                    /* Alert */
                    $subject->createAlert(new Alert(
                        Alert::LEVEL_WARNING,
                        "visit_date_changed",
                        "Sujet ".$subject->getCompleteId()." : la date de la visite ".$visit->getPublicDescription()." a changé : était ".$old_visit_date->format(DATE_FORMAT_UI)." et est maintenant ".$visit->getDate()->format(DATE_FORMAT_UI)),
                                          $alerts);
                }
            }


            /* Survey status */
            /* Only if survey exists in LimeSurvey data (Id not null) */
            if ($visit->getSurvey() and $visit->getSurvey()->getId()){
                /* Check if survey started (status different than NOT_DONE) whereas visit hasn't been done */
                if (in_array($visit->getStatus(), array(Visit::STATUS_NOT_SCHEDULED, Visit::STATUS_SCHEDULED, Visit::STATUS_COMPLETED_WO_SURVEY))
                    and $visit->getSurvey()->getStatus() != Survey::STATUS_NOT_DONE){
                    /* Alert */
                    $subject->createAlert(new Alert(
                        Alert::LEVEL_WARNING,
                        "visit_survey_status_inconsistent",
                        "Sujet ".$subject->getCompleteId()." : le statut \"".$visit->getStatusDescription()."\" de la visite ".$visit->getPublicDescription()." est incohérent avec celui du questionnaire \"".$visit->getSurvey()->getStatusDescription()."\""),
                                          $alerts);
                }
                /* Check if survey started (status different than NOT_DONE) whereas visit was canceled. In this case, set survey status to canceled */
                else if ($visit->getStatus() == Visit::STATUS_CANCELED){
                    if ($visit->getSurvey()->getStatus() != Survey::STATUS_NOT_DONE){
                        /* Alert */
                        $subject->createAlert(new Alert(
                            Alert::LEVEL_WARNING,
                            "visit_survey_status_inconsistent",
                            "Sujet ".$subject->getCompleteId()." : le statut \"".$visit->getStatusDescription()."\" de la visite ".$visit->getPublicDescription()." est incohérent avec celui du questionnaire \"".$visit->getSurvey()->getStatusDescription()."\""),
                                              $alerts);
                    }
                    $visit->getSurvey()->setStatus(Survey::STATUS_CANCELED);
                }
                /* Check if survey started before visit */
                else if (in_array($visit->getSurvey()->getStatus(), array(Survey::STATUS_STARTED, Survey::STATUS_COMPLETED))
                         and $visit->getSurvey()->getStartDate() < $visit->getDate()){
                    /* Alert */
                    $subject->createAlert(new Alert(
                        Alert::LEVEL_WARNING,
                        "visit_survey_started_before",
                        "Sujet ".$subject->getCompleteId()." : le questionnaire de la visite ".$visit->getPublicDescription()." a été débuté avant la visite (date : ".$visit->getSurvey()->getStartDate()->format(DATE_FORMAT_UI).")"),
                                          $alerts);
                }

                /* Compare name initials in survey and subject name (accent removed, upper case) */
                if (defined("LS_Q_SUBJECT_NAME_INITIALS")
                    and in_array($visit->getSurvey()->getStatus(), array(Survey::STATUS_STARTED, Survey::STATUS_COMPLETED))){
                    $transliterator = Transliterator::create('NFD; [:Nonspacing Mark:] Remove; NFC');
                    $subject_name_initials = strtoupper(substr($transliterator->transliterate($subject->getFirstname()), 0, 1).substr($transliterator->transliterate($subject->getSurname()), 0, 1));
                    if ($subject_name_initials != $visit->getSurvey()->getSubjectNameInitials()){
                        $subject->createAlert(new Alert(
                            Alert::LEVEL_WARNING,
                            "visit_survey_wrong_name_initials",
                            "Sujet ".$subject->getCompleteId()." : les initiales \"".$visit->getSurvey()->getSubjectNameInitials()."\" renseignées dans le questionnaire de la visite ".$visit->getPublicDescription()." ne correspondent pas au nom du sujet (initiales : ".$subject_name_initials.")"),
                                              $alerts);
                    }
                }
            }

            /* Remember visit date for comparison with next visit */
            if ($visit->isNormal()){
                $previous_visit_date = $visit->getDate();
            }
        }
    }

    /* Alert if non-attached surveys */
    foreach ($surveys as $survey){
        $alerts[] = new Alert(
            Alert::LEVEL_ERROR,
            "survey_unattached",
            "Le questionnaire avec le token ".$survey->getToken()." n'est associé à aucune visite d'aucun sujet");
    }

    /* Alerts concerning orphan appointments */
    foreach ($appointments as $appointment){
        /* Unknown subject ID */
        if ($appointment->getSubjectId()){
            $alerts[] = new Alert(
                Alert::LEVEL_WARNING,
                "appointment_orphan_unknown_subject_ID",
                "Le rendez-vous orphelin du ".$appointment->getDate()->format(DATE_FORMAT_UI)." avec l'investigateur ".$appointment->getInvestigatorName()." est associé à un ID sujet inconnu : ".$appointment->getSubjectId());
        }
        /* In the past */
        else if ($appointment->getDate() < new DateTime()){
            $alerts[] = new Alert(
                Alert::LEVEL_WARNING,
                "appointment_orphan_past",
                "Le rendez-vous orphelin du ".$appointment->getDate()->format(DATE_FORMAT_UI)." avec l'investigateur ".$appointment->getInvestigatorName()." est passé");
        }
    }

    return 0;
}



/*******************/
/* Alerts handling */
/*******************/

function send_alerts(array $alerts, array $subjects){
    usort($alerts, function($alert1, $alert2){
        if ($alert1->getLevel() != $alert2->getLevel()){
            return ($alert1->getLevel() > $alert2->getLevel()) ? -1 : 1;
        }
        return ($alert1->getDate() < $alert2->getDate()) ? -1 : 1;
    });

    $emails = array();

    foreach ($alerts as $alert){
        /* DEBUG */
        print_output($alert->__toString());

        /* Message for admin */
        $recipients = $alert->getRecipients();
        if ($recipients and $alert->getMessageAdmin()){
            if (! array_key_exists($recipients, $emails)){
                $emails[$recipients] = array();
            }
            $emails[$recipients][] = "*".ucfirst($alert->getLevelDescription())."* : ".$alert->getMessageAdmin();
        }
        /* Message for subject */
        if ($alert->getMessageSubject()){
            $subject = $alert->getSubject($subjects);
            if (! $subject){
                add_log("No subject found for alert ".$alert->__toString());
                break;
            }
            $subject_email = $subject->getEmail();
            if ($subject_email){
                if (! array_key_exists($recipients, $emails)){
                    $emails[$subject_email] = array();
                }
                $emails[$subject_email][] = $alert->getMessageSubject();
            }
        }
    }

    foreach ($emails as $recipients => $messages){
        $email_subject = "Etude HERBAS : notification importante";
        $email_message = "Bonjour.\r\n\r\n"
                       ."Ci-dessous une alerte déclenchée par l'algorithme de vérification des données de l'étude HERBAS.\r\n\r\n- "
                       .implode("\r\n\r\n- ", $messages);

        $email_header = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDRESS.">\r\n"
                      ."Reply-To: ".EMAIL_REPLY_TO."\r\n"
                      ."Content-Type: text/plain; charset=utf-8\r\n"
                      ."Content-Transfer-Encoding: quoted-printable";

        $email_content = quoted_printable_encode($email_message);

        /* Sending mail only if MODE = prod. Otherwise, display mail */
        if (MODE == "dev"){
            print_output(array($recipients, $email_subject, $email_content, $email_header));
        }
        else {
            if (! mail($recipients, $email_subject, $email_content, $email_header)) {
                add_log("Alert email sending to ".$recipients." failed");
            }
        }
    }

    return 0;
}


/***********/
/* Display */
/***********/

/**
 * Compute integer value from first numbers contained in an ID
 * @param   string   $id input ID
 * @return  int
 */
function get_int_from_id($id){
    preg_match("#[0-9]+#", $id, $result);
    if (! $result[0]){
        add_log("No integer ID can be compute from id ".$id);
        return 0;
    }
    return intval($result[0]);
}
