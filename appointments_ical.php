<?php

include_once("config.php");
include_once("functions.php");

if (! isset($_REQUEST["key"])){
    http_response_code(403);
    exit;
}

$investigator_ids = null;
$user_name = "";
foreach ($users as $user){
    if (isset($user["ical"])){
        foreach ($user["ical"] as $ical_config){
            if ($ical_config["key"] == $_REQUEST["key"]){
                $investigator_ids = $ical_config["EA_provider_ids"];
                $user_name = $user["name"];
                break;
            }
        }
        if ($user_name){
            break;
        }
    }
}
if (! $investigator_ids){
    http_response_code(403);
    exit;
}

$appointments = load_appointments();
/* DEBUG */
/* print_output($appointments); */
/* exit; */

foreach ($appointments as $key => $appointment){
    if (! in_array($appointment->getInvestigatorId(), $investigator_ids)){
        unset($appointments[$key]);
    }
}
header("Content-Type: text/calendar; charset=utf-8");
?>
BEGIN:VCALENDAR
CALSCALE:GREGORIAN
PRODID: HERBAS inclusion manager
VERSION:2.0
X-WR-CALNAME;VALUE=TEXT:<?php echo "HERBAS user "
                                    .$user_name
                                    ." investigators ".implode("-", $investigator_ids)
                                    .PHP_EOL; ?>
X-WR-RELCALID:<?php echo "HERBAS user "
                        .$user_name
                        ." investigators ".implode("-", $investigator_ids)
                        .PHP_EOL; ?>
<?php
foreach ($appointments as $appointment){
?>
BEGIN:VEVENT
SUMMARY:<?php echo $appointment->getSubjectFirstname()
            ." ".strtoupper($appointment->getSubjectSurname())
            .PHP_EOL; ?>
DESCRIPTION:<?php echo "Investigateur : ".$appointment->getInvestigatorName()
                    ."\\n\n  Statut : ".$appointment->getStatusDescription().PHP_EOL; ?>
UID:HERBAS_Appointement_<?php echo $appointment->getId().PHP_EOL; ?>
DTSTAMP:<?php echo $appointment->getDate()->format(DATE_FORMAT_ICAL).PHP_EOL; ?>
DTSTART:<?php echo $appointment->getDate()->format(DATE_FORMAT_ICAL).PHP_EOL; ?>
DTEND:<?php echo $appointment->getEndDate()->format(DATE_FORMAT_ICAL).PHP_EOL; ?>
LOCATION:Bureau investigation HERBAS
STATUS:<?php echo ($appointment->getStatus() == Appointment::STATUS_CANCELED) ? "CANCELLED" : "CONFIRMED"; echo PHP_EOL; ?>
END:VEVENT
<?php
}
?>
END:VCALENDAR
